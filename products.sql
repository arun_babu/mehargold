-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 22, 2020 at 01:51 PM
-- Server version: 5.6.47-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mehargold`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `products_id` int(11) NOT NULL,
  `products_quantity` int(11) NOT NULL,
  `products_model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `products_image` text COLLATE utf8mb4_unicode_ci,
  `products_video_link` text COLLATE utf8mb4_unicode_ci,
  `products_price` decimal(15,2) NOT NULL,
  `products_date_added` datetime NOT NULL,
  `products_last_modified` datetime DEFAULT NULL,
  `products_date_available` datetime DEFAULT NULL,
  `products_weight` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `products_weight_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `products_status` tinyint(1) NOT NULL,
  `is_current` tinyint(1) NOT NULL,
  `products_tax_class_id` int(11) NOT NULL,
  `manufacturers_id` int(11) DEFAULT NULL,
  `products_ordered` int(11) NOT NULL DEFAULT '0',
  `products_liked` int(11) NOT NULL,
  `low_limit` int(11) NOT NULL,
  `is_feature` tinyint(1) NOT NULL DEFAULT '0',
  `products_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `products_type` int(11) NOT NULL DEFAULT '0',
  `products_min_order` int(11) NOT NULL DEFAULT '1',
  `products_max_stock` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `currency` char(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`products_id`, `products_quantity`, `products_model`, `products_image`, `products_video_link`, `products_price`, `products_date_added`, `products_last_modified`, `products_date_available`, `products_weight`, `products_weight_unit`, `products_status`, `is_current`, `products_tax_class_id`, `manufacturers_id`, `products_ordered`, `products_liked`, `low_limit`, `is_feature`, `products_slug`, `products_type`, `products_min_order`, `products_max_stock`, `created_at`, `updated_at`, `currency`, `product_code`, `image`) VALUES
(1, 0, 'fresh', '412', NULL, '85.00', '2020-04-05 01:10:00', NULL, NULL, '0.500', NULL, 1, 1, 0, NULL, 0, 1, 0, 1, 'casual-tracksuit-children-boy', 0, 1, 0, '2019-09-17 18:06:27', '2019-10-01 19:17:25', '', '', NULL),
(2, 0, 'fresh', '408', NULL, '60.00', '2020-04-05 01:10:00', NULL, NULL, '0.370', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'fashion-plaid-boy-clothing-set', 0, 1, 0, '2019-09-17 18:20:43', '2019-10-01 19:16:55', '', '', NULL),
(3, 0, 'fresh', '405', NULL, '55.00', '2020-04-05 01:10:00', NULL, NULL, '0.800', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'cosplay-minnie-mouse-dress', 0, 1, 0, '2019-09-17 18:28:04', '2019-10-01 19:16:24', '', '', NULL),
(4, 0, NULL, '400', NULL, '90.00', '2020-04-05 01:10:00', NULL, NULL, '1.00', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'princess-flower-girl-dress', 1, 1, 0, '2019-09-17 18:31:56', '2019-10-01 19:15:47', '', '', NULL),
(5, 0, 'fresh', '395', NULL, '45.00', '2020-04-05 01:10:00', NULL, NULL, '0.200', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'casual-breathable-outdoor-kids-sneakers', 0, 1, 0, '2019-09-17 18:51:32', '2019-10-01 19:15:21', '', '', NULL),
(6, 0, 'fresh', '390', NULL, '30.00', '2020-04-05 01:10:00', NULL, NULL, '0.150', NULL, 1, 1, 0, NULL, 0, 0, 0, 1, 'alloy-military-quartz-watch-man', 0, 1, 0, '2019-09-17 19:01:11', '2019-10-01 19:14:48', '', '', NULL),
(7, -3, 'fresh', '385', NULL, '25.00', '2020-04-05 01:10:00', NULL, NULL, '0.125', NULL, 1, 1, 0, NULL, 3, 0, 0, 1, 'men-s-cotton-classic-baseball-cap', 0, 1, 0, '2019-09-17 19:18:31', '2020-04-02 18:12:24', '', '', NULL),
(8, 0, 'fresh', '383', NULL, '32.00', '2020-04-05 01:10:00', NULL, NULL, '0.225', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'carrot-fitt-jeans', 1, 1, 0, '2019-09-17 19:24:49', '2019-10-01 19:10:52', '', '', NULL),
(9, 0, 'fresh', '378', NULL, '15.00', '2020-04-05 01:10:00', NULL, NULL, '0.125', NULL, 1, 1, 0, NULL, 0, 0, 0, 1, 'casual-cotton-jeans-for-men', 0, 1, 0, '2019-09-17 19:30:05', '2019-10-01 19:09:48', '', '', NULL),
(10, 0, 'fresh', '375', NULL, '28.00', '2020-04-05 01:10:00', NULL, NULL, '0.125', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'super-skinny-fitt-jeans', 0, 1, 0, '2019-09-17 19:39:51', '2019-10-01 19:07:51', '', '', NULL),
(11, 0, 'fresh', '372', NULL, '21.00', '2020-04-05 01:10:00', NULL, NULL, '0.100', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'dotted-casual-shirt-for-men', 1, 1, 0, '2019-09-17 19:45:49', '2019-10-01 19:05:34', '', '', NULL),
(12, 0, NULL, '367', NULL, '18.00', '2020-04-05 01:10:00', NULL, NULL, '0.100', NULL, 1, 1, 0, NULL, 0, 1, 0, 0, 'juventus-henley-neck-tshirt', 0, 1, 0, '2019-09-17 19:50:49', '2019-10-01 19:04:50', '', '', NULL),
(13, 0, NULL, '364', NULL, '34.00', '2020-04-05 01:10:00', NULL, NULL, '0.100', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'men-polo-casual-shirt', 1, 1, 0, '2019-09-17 19:55:20', '2019-10-01 19:02:22', '', '', NULL),
(14, 0, 'fresh', '359', NULL, '39.99', '2020-04-05 01:10:00', NULL, NULL, '0.450', NULL, 1, 1, 0, NULL, 0, 0, 0, 1, 'brock-knitted-mesh-casual-oxford-sneakers', 0, 1, 0, '2019-09-17 08:00:29', '2019-10-01 19:01:22', '', '', NULL),
(15, 0, NULL, '355', NULL, '48.00', '2020-04-05 01:10:00', NULL, NULL, '0.350', NULL, 1, 1, 0, NULL, 0, 1, 0, 0, 'light-man-lace-up-shoes', 0, 1, 0, '2019-09-17 08:03:15', '2019-10-01 19:00:03', '', '', NULL),
(16, -1, NULL, '350', NULL, '52.00', '2020-04-05 01:10:00', NULL, NULL, '0.500', NULL, 1, 1, 0, NULL, 1, 0, 0, 0, 'mesh-breathable-men-s-sneakers', 0, 1, 0, '2019-09-17 08:07:07', '2019-10-01 18:59:13', '', '', NULL),
(17, 0, NULL, '344', NULL, '19.00', '2020-04-05 01:10:00', NULL, NULL, '0.125', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'fashion-foldable-beach-sun-hat', 0, 1, 0, '2019-09-17 08:13:05', '2019-10-01 18:58:31', '', '', NULL),
(18, 0, NULL, '339', NULL, '36.99', '2020-04-05 01:10:00', NULL, NULL, '0.200', NULL, 1, 1, 0, NULL, 0, 0, 0, 1, 'women-magnet-buckle-quartz-watch', 0, 1, 0, '2019-09-17 08:15:40', '2019-10-01 18:58:01', '', '', NULL),
(19, 0, NULL, '337', NULL, '23.00', '2020-04-05 01:10:00', NULL, NULL, '0.100', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'cotton-skirt-for-women', 0, 1, 0, '2019-09-17 08:20:25', '2019-10-01 18:57:29', '', '', NULL),
(20, 0, NULL, '333', NULL, '90.00', '2019-09-18 07:10:39', NULL, NULL, '0.300', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'cotton-skirt-with-buttons', 0, 1, 0, '2019-09-17 08:23:37', '2019-10-01 18:56:36', '', '', NULL),
(21, 0, NULL, '330', NULL, '77.00', '2019-09-18 07:10:39', NULL, NULL, '0.600', NULL, 1, 1, 0, NULL, 0, 1, 0, 1, 'mid-waist-culottes-pent', 1, 1, 0, '2019-09-17 08:56:06', '2019-10-01 18:55:45', '', '', NULL),
(22, 0, NULL, '324', NULL, '96.00', '2019-09-18 07:10:39', NULL, NULL, '0.600', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'crystal-high-heels', 0, 1, 0, '2019-09-17 09:01:42', '2019-09-24 17:19:34', '', '', NULL),
(24, 0, NULL, '319', NULL, '87.00', '2019-09-18 07:10:39', NULL, NULL, '0.550', NULL, 1, 1, 0, NULL, 0, 1, 0, 0, 'party-dinner-shoes-woman', 1, 1, 0, '2019-09-17 09:11:17', '2019-10-01 18:55:03', '', '', NULL),
(25, 0, NULL, '313', NULL, '73.00', '2019-09-18 07:10:39', NULL, NULL, '0.525', NULL, 1, 1, 0, NULL, 0, 0, 0, 1, 'women-pumps-ankle-strap', 0, 1, 0, '2019-09-17 09:15:09', '2019-10-01 18:54:23', '', '', NULL),
(26, 0, 'fresh', '310', NULL, '105.00', '2019-09-18 07:10:39', NULL, NULL, '0.525', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'button-up-shoulder-top', 0, 1, 0, '2019-09-18 13:35:14', '2020-03-04 09:29:19', '', '', NULL),
(27, 0, NULL, '307', NULL, '75.00', '2019-09-18 07:10:39', NULL, NULL, '0.400', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'double-tank-shirt-for-women', 0, 1, 0, '2019-09-18 13:40:38', '2019-10-01 18:53:00', '', '', NULL),
(28, 0, NULL, '301', NULL, '92.00', '2019-09-18 07:10:39', NULL, NULL, '0.400', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'printed-rose-petal-shirt', 1, 1, 0, '2019-09-18 13:45:49', '2019-10-01 18:52:07', '', '', NULL),
(29, -1, NULL, '300', NULL, '79.00', '2019-09-18 07:10:39', NULL, NULL, '0.400', NULL, 1, 1, 0, NULL, 1, 1, 0, 0, 'strip-knitwear-for-women', 0, 1, 0, '2019-09-18 13:50:40', '2019-10-01 18:51:36', '', '', NULL),
(30, 0, NULL, '297', NULL, '130.00', '2019-09-18 07:10:39', NULL, NULL, '0.750', NULL, 1, 1, 0, NULL, 0, 1, 0, 0, 'denim-jacket-reverse', 0, 1, 0, '2019-09-18 14:00:31', '2019-10-01 18:51:05', '', '', NULL),
(31, 0, NULL, '292', NULL, '67.00', '2019-09-18 07:10:39', NULL, NULL, '0.800', NULL, 1, 1, 0, NULL, 0, 1, 0, 0, 'quilted-gilet-hoodie', 0, 1, 0, '2019-09-18 14:05:41', '2019-10-01 18:50:24', '', '', NULL),
(32, -3, NULL, '288', NULL, '81.00', '2019-09-18 07:10:39', NULL, NULL, '0.900', NULL, 1, 1, 0, NULL, 5, 3, 0, 0, 'straight-long-coat', 0, 1, 0, '2019-09-18 14:10:39', '2020-04-02 13:08:37', '', '', NULL),
(36, 0, 'nn', '519', NULL, '45.00', '0000-00-00 00:00:00', NULL, NULL, '10', NULL, 1, 1, 2, NULL, 0, 1, 0, 1, 'suite', 1, 1, 10, '2020-06-01 12:53:43', NULL, '', '', NULL),
(37, 0, NULL, '540', NULL, '250.00', '0000-00-00 00:00:00', NULL, NULL, '10', NULL, 1, 1, 2, NULL, 0, 0, 0, 1, 'american-diamond-stud-earrings', 1, 1, NULL, '2020-06-08 15:20:35', '2020-06-08 15:24:17', '', '', NULL),
(38, 0, NULL, '542', NULL, '250.00', '0000-00-00 00:00:00', NULL, NULL, '10', NULL, 1, 1, 0, NULL, 0, 0, 0, 1, 'a-ring', 1, 1, NULL, '2020-06-10 11:42:17', '2020-06-10 11:54:11', '', '', NULL),
(39, 0, 'ADJESTABILE BANGIL', '542', NULL, '200.00', '0000-00-00 00:00:00', NULL, NULL, '10', NULL, 1, 1, 0, NULL, 0, 1, 0, 1, 'palakka-bangil', 0, 22, 25, '2020-06-10 13:23:17', NULL, '', '', NULL),
(40, 0, NULL, '546', NULL, '12.00', '0000-00-00 00:00:00', NULL, NULL, '12', NULL, 1, 1, 0, NULL, 0, 0, 0, 0, 'das', 0, 1, NULL, '2020-06-22 18:33:17', '2020-06-22 18:38:15', '', '', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`products_id`),
  ADD KEY `idx_products_model` (`products_model`),
  ADD KEY `idx_products_date_added` (`products_date_added`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `products_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
