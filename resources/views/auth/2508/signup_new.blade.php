<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>BADA</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" href="{{asset('landing/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/style.css')}}">

</head>

<body>

<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="collapse navbar-collapse d-md-block d-none" id="navbarNav">
                        <ul class="navbar-nav frst-ul">
                            <li class="nav-item">
                                <a class="nav-link" href="/">HOME</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="signup">BECOME A PRTNER</a>
                            </li>
                        </ul>
                    </div>
                    <a class="navbar-brand" href="/">
                        <img src="{{asset('landing/images/logo.svg')}}" alt="">
                        <span></span>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav scnd-ul">

                            <li class="nav-item d-block d-md-none">
                                <a class="nav-link" href="partner.html">BECOME A PRTNER</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">CONTACT US</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="login">LOGIN</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="register-form">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Register with Bada Business</h1>
            </div>
            <div class="col-md-6">
                <!-- progressbar -->
                <ul id="progressbar">
                    <li class="active" data-value="1"></li>
                    <li data-value="2"></li>
                    <li data-value="3"></li>
                    <li data-value="4"></li>
                    <li data-value="5"></li>
                </ul>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="form-section">
                    <!-- multistep form -->
                    <div class="list-error"></div>
                    <form name="signup" enctype="multipart/form-data" action="{{ URL::to('/signupProcess')}}"
                          id="msform"
                          class="signup"
                          method="post">
                    {{csrf_field()}}

                    <!-- fieldsets -->
                        <fieldset>
                            <h6>Personal Information</h6>
                            <input type="text" name="firstName" placeholder="First Name">
                            <input type="text" name="lastName" placeholder="Last Name">
                            <input type="number" name="phone" id="phone" placeholder="Mobile">
                            <input type="email" name="email" placeholder="Email">
                            <input type="password" name="password" placeholder="Password">
                            <input type="password" name="re_password" placeholder="Conform Password">
                            <select name="gender" id="">
                                <option value="">Gender</option>
                                <option value="2">Male</option>
                                <option value="1">Female</option>
                            </select>
                            <input type="checkbox" class="checkbox" id="" name="" value="">
                            <label class="check-label" for=""> I agree <a href=""> Terms & Conditions</a></label>
                            <label class="login-label">Already have and account? <a href="login">Login</a></label>
                            <button type="button" name="next" class="next-custom action-button submit-signup" value="" disabled="true">
                                Next<span></span>
                            </button>
                        </fieldset>
                        <fieldset>
                            <h6>OTP Verification</h6>
                            <input type="number" name="" placeholder="Enter OTP" class="verification_code">
                            <button type="button" name="next" class=" action-button scnd-btn button-verification"
                                    value="">
                                Next<span></span>
                            </button>
                            <label class="login-label scnd-field">Didn't get OTP? <a href="">Resend</a></label>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <a href="index.html">
                    <img src="{{asset('landing/images/footer-logo.svg')}}" class="img-fluid" alt="">
                </a>
                <p>Join India’s #1 Reselling platform trusted by 1 Crore+ Resellers who are earning more than
                    ₹25,000 every month!</p>
            </div>
            <div class="col-md-3">
                <h4>Company</h4>
                <ul>
                    <li><a href="">Careers</a></li>
                    <li><a href="">Bada Business Blog</a></li>
                    <li><a href="">Mehar Tech Blog</a></li>
                    <li><a href="">T&C</a></li>
                    <li><a href="">Privacy</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <h4>Contact Us</h4>
                <ul>
                    <li><a href="">+91 1234 567 9999</a></li>
                    <li><a href="">help@badabusiness.com</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="border-bottem"></div>
            </div>
            <div class="col-md-6">
                <div class="social-media">
                    <a href=""><img src="{{asset('landing/images/fb.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/insta.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/tw.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/in.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/YT.png')}}" class="img-fluid" alt=""></a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="copy-right">
                    <p>© 2015-20 Bada Business Inc. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="{{asset('landing/js/jquery-3.4.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('landing/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('landing/js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('landing')}}" type="text/javascript"></script>
<script src="{{asset('landing/js/slick.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    AOS.init();
</script>
<script type="text/javascript">
    function uploadFile(target) {
        document.getElementById("file-name").innerHTML = target.files[0].name;
    }

    $('.submit-signup').click(function (e) {
        // e.preventDefault();
        if (true) {
            var formData = $('.signup').serialize();
            $.ajax({
                url: '/validate',
                type: "POST",
                data: formData,
                success: function (res) {
                    console.log(res.error, 'res.error')
                    var error = '';
                    if (res.error != 'null') {
                        $.each(res.error, function (index, value) {
                            error += '<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
                                '                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n' +
                                '                                <span class="sr-only">Error:</span>\n' +
                                '                                ' + value + '\n' +
                                '                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                                '                                    <span aria-hidden="true">&times;</span>\n' +
                                '                                </button>\n' +
                                '                            </div>';
                        });
                    } else {


                        var current_fs, next_fs, previous_fs; //fieldsets
                        var opacity;


                        current_fs = $('.next-custom').parent();
                        next_fs = $('.next-custom').parent().next();
                        prev_fs = $('.next-custom').parent().prev();

                        active_step_val = current_fs.length;
                        //  console.log(index(next_fs));

                        //Add Class Active
                        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                        // adding new class to already done radios
                        $("#progressbar li").eq($("fieldset").index(current_fs)).addClass("step_done");


                        //show the next fieldset
                        next_fs.show();
                        //hide the current fieldset with style
                        current_fs.animate({opacity: 0}, {
                            step: function (now) {
                                // for making fielset appear animation
                                opacity = 1 - now;

                                current_fs.css({
                                    'display': 'none',
                                    'position': 'relative'
                                });
                                next_fs.css({'opacity': opacity});
                            },
                            // duration: 600
                        });
                        var phone = $('#phone').val();

                        $.ajax({
                            url: '/verify',
                            type: "POST",
                            data: {phone: phone, "_token": "{{ csrf_token() }}",},
                            success: function (res) {
                                if (res.status) {
                                    // $('.register-form').css('display', 'none');
                                    // $('.mobile-verification').css('display', '');
                                } else {
                                    error += '<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
                                        '                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n' +
                                        '                                <span class="sr-only">Error:</span>\n' +
                                        '                                Something went wrong!\n' +
                                        '                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                                        '                                    <span aria-hidden="true">&times;</span>\n' +
                                        '                                </button>\n' +
                                        '                            </div>';
                                }
                            },
                        });
                    }
                    $('.list-error').html(error);
                },
            });
        }

    });
    $('.button-verification').click(function () {
        var phone = $('#phone').val();
        var verification_code = $('.verification_code').val();
        $.ajax({
            url: '/confirm',
            type: "POST",
            data: {phone: phone, verification_code: verification_code, "_token": "{{ csrf_token() }}"},
            success: function (res) {
                if (res.status) {
                    $('.signup').submit();
                }

            },
        });
    })
    $('.checkbox').click(function () {
        if ($(this).prop("checked") == true)
            $('.next-custom').attr('disabled', false)
        else
            $('.next-custom').attr('disabled', true)
    })
</script>

<script>
    $(document).ready(function () {

        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;


        $('.radio-group .radio').click(function () {
            $(this).parent().find('.radio').removeClass('selected');
            $(this).addClass('selected');
        });

        $(".submit").click(function () {
            return false;
        })

    });
</script>

</body>

</html>