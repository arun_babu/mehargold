<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>BADA</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" href="{{asset('landing/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/style.css')}}">
    <style>
        .custom-button:hover .custom-menu{
            display: block !important;

        }
    </style>

</head>

<body>

<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="collapse navbar-collapse d-md-block d-none" id="navbarNav">
                        <div class="navbar-lang">
                            @if(count($languages) > 1)
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle custom-button" type="button">
                                        {{session('language_name')}}
                                    </button>
                                    <div class="dropdown-menu custom-menu">
                                        @foreach($languages as $language)
                                            <a class="dropdown-item" onclick="myFunction1({{$language->languages_id}})"
                                               href="#">
                                                {{$language->name}}</a>
                                        @endforeach
                                    </div>
                                </div>
                                @include('web.common.scripts.changeLanguage')
                            @endif
                        </div>
                        <ul class="navbar-nav frst-ul">
                            <li class="nav-item">
                                <a class="nav-link" href="/">@lang('website.HOME')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="signup">@lang('website.BECOME A PRTNER')</a>
                            </li>
                        </ul>
                    </div>
                    <a class="navbar-brand" href="/">
                        <img src="{{asset('landing/images/logo.svg')}}" alt="">
                        <span></span>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav scnd-ul">
                            <li class="nav-item d-block d-md-none">
                                <a class="nav-link" href="partner.html">@lang('website.BECOME A PRTNER')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact">@lang('website.CONTACT US')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="login">@lang('website.LOGIN')</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>@lang('website.Earn money online').<br> @lang('website.START YOUR BADA BUSINESS TODAY')!</h1>
                <h5>@lang('website.Sell Resell Work from home')</h5>
                <p>@lang('website.Sell on WhatsApp, Facebook and Instagram') </p>
                <p>@lang('website.Sell Resell Work from home')</p>
                <div class="link-form d-none d-md-block">
                    <form>
                        <div class="std-code">+91</div>
                        <input type="text" class="form-input" name="phone" placeholder="@lang('website.Please enter your Phone Number')">
                        <button type="submit" class="submit-button">Get App Link</button>
                    </form>
                </div>
                <div class="links">
                    <a href="https://play.google.com/store/apps/details?id=com.mehar.meharbadabusiness" target="_blank">
                        <img src="{{asset('landing/images/google-play-btn.png')}}" class="img-fluid" alt="">
                    </a>
                    <a href="">
                        <img src="{{asset('landing/images/play-store-btn.png')}}" class="img-fluid" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="leading-jewelery">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>@lang('website.India s Leading jewelry manufacturers')</h1>
                <p> @lang('website.des line1')<br
                            class="d-none d-md-block"> @lang('website.des line2')<br class="d-none d-md-block">
                </p>
                <p> @lang('website.des line3')
                    <br class="d-none d-md-block">
                    @lang('website.des line4')
                </p>
            </div>
        </div>
        <div class="count-number">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6 text-md-right">
                            <img src="{{asset('landing/images/Icons-1.svg')}}" class="img-fluid" alt="">
                        </div>
                        <div class="col-md-6">
                            <div class="content">
                                <h6>10000+</h6>
                                <p>@lang('website.Happy Women Resellers')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6 text-md-right">
                            <img src="{{asset('landing/images/Icons-2.svg')}}" class="img-fluid" alt="">
                        </div>
                        <div class="col-md-6">
                            <div class="content">
                                <h6>10000+</h6>
                                <p>@lang('website.Trendy Models')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6 text-md-right">
                            <img src="{{asset('landing/images/Icons-3.svg')}}" class="img-fluid" alt="">
                        </div>
                        <div class="col-md-6">
                            <div class="content">
                                <h6>2 Lakhs +</h6>
                                <p>100% @lang('website.Satisfied Costumers')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="reselling-business">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>@lang('website.Start Reselling with')<br> @lang('website.Mehar Business in 3 Simple Steps')</h1>
                <p>@lang('website.Our top Resellers come from metros')</p>
            </div>
        </div>
        <div class="business-content">
            <div class="row">
                <div class="col-md-4">
                    <div class="dspn-one">
                        <h5>@lang('website.Download and Browse')</h5>
                        <p>@lang('website.Download the app,')<br class="d-none d-md-block">
                            @lang('website.Select your Products')</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dspn-two">
                        <h5>@lang('website.Share and Place Order')</h5>
                        <p>@lang('website.Set, your Profit and share through Whatsapp,')<br class="d-none d-md-block"> @lang('website.Facebook and Instagram etc').<br class="d-none d-md-block">
                            @lang('website.Place Order for your Customers')</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dspn-three">
                        <h5>@lang('website.Sell and Earn Money')</h5>
                        <p>@lang('website.Earn Commission for each Sale').<br class="d-none d-md-block"> @lang('website.Get Extra Bonuses and Surprise Gifts')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="live-your-dream">
    <div class="container">
        <div class="row">
            <div class="col-md-12 margin-top">
                <a href="" data-toggle="modal" data-target="#exampleModal-">
                    <div class="video text-center">
                        <img src="{{asset('landing/images/video.png')}}" class="img-fluid" alt="">
                    </div>
                    <div class="content">
                        <h2>@lang('website.HOW TO EARN WITH') <br> <span>@lang('website.MEHAR BADA BUSINESS')</span></h2>
                    </div>
                    <div class="content-two">
                        <img src="{{asset('landing/images/videologo.svg')}}" class="img-fluid" alt="">
                    </div>
                    <div class="content-three">
                        <h5>@lang('website.Bada Business Solutions')</h5>
                        <p>@lang('website.www.badabusiness.com')</p>
                    </div>
                </a>
            </div>
            <div class="col-md-6 text-center order-md-0 order-1">
                <img src="{{asset('landing/images/Lady.png')}}" class="img-fluid lady-img" alt="">
            </div>
            <div class="col-md-6 order-md-1 order-0">
                <h1>@lang('website.Live Your Dreams')!<br> @lang('website.Be Bold & Independent')</h1>
                <p>@lang('website.Live Your Dreams des')</p>
            </div>
        </div>
    </div>
</div>
<div class="nolimit-business">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>@lang('website.There is No Limit to How Much')<br> @lang('website.You can Earn with Bada Business')!</h1>
            </div>
        </div>
        <div class="limit-content">
            <div class="row">
                <div class="col-md-4">
                    <div class="dspn-one">
                        <p>@lang('website.Earn Margins more than')</p>
                        <h5>₹30,000 @lang('website.monthly')</h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dspn-two">
                        <p>@lang('website.Get Bonus upto')</p>
                        <h5>₹6,000 @lang('website.weekly')</h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dspn-three">
                        <p>@lang('website.Earn Commission upto')</p>
                        <h5>₹5,000 @lang('website.per referral')</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="anyone-can-resell">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>@lang('website.Anyone can Resell with Bada Business'    )</h1>
                <p>@lang('website.resell des')</p>
            </div>
        </div>
        <div class="anyone-content d-none d-md-block" id="slider">
            <div class="item show">
                <div class="row">
                    <div class="col-md-3">
                        <div class="over-width">
                            <img src="{{asset('landing/images/anyone-img-1.png')}}" class="img-fluid" alt="">
                            <div class="overlay"></div>
                        </div>
                        <div class="dspn">
                            <h6>Sara James</h6>
                            <p>Lorem Ipsum dolor</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="over-width">
                            <img src="{{asset('landing/images/anyone-img-2.png')}}" class="img-fluid" alt="">
                            <div class="overlay"></div>
                        </div>
                        <div class="dspn">
                            <h6>Ann Mathew</h6>
                            <p>Lorem Ipsum dolor</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="over-width">
                            <img src="{{asset('landing/images/anyone-img-3.png')}}" class="img-fluid" alt="">
                            <div class="overlay"></div>
                        </div>
                        <div class="dspn">
                            <h6>Maria Eisac</h6>
                            <p>Lorem Ipsum dolor</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="over-width">
                            <img src="{{asset('landing/images/anyone-img-4.png')}}" class="img-fluid" alt="">
                            <div class="overlay"></div>
                        </div>
                        <div class="dspn">
                            <h6>Jassic John</h6>
                            <p>Lorem Ipsum dolor</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-3">
                        <div class="over-width">
                            <img src="{{asset('landing/images/anyone-img-2.png')}}" class="img-fluid" alt="">
                            <div class="overlay"></div>
                        </div>
                        <div class="dspn">
                            <h6>Ann Mathew</h6>
                            <p>Lorem Ipsum dolor</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="over-width">
                            <img src="{{asset('landing/images/anyone-img-1.png')}}" class="img-fluid" alt="">
                            <div class="overlay"></div>
                        </div>
                        <div class="dspn">
                            <h6>Sara James</h6>
                            <p>Lorem Ipsum dolor</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="over-width">
                            <img src="{{asset('landing/images/anyone-img-4.png')}}" class="img-fluid" alt="">
                            <div class="overlay"></div>
                        </div>
                        <div class="dspn">
                            <h6>Jassic John</h6>
                            <p>Lorem Ipsum dolor</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="over-width">
                            <img src="{{asset('landing/images/anyone-img-3.png')}}" class="img-fluid" alt="">
                            <div class="overlay"></div>
                        </div>
                        <div class="dspn">
                            <h6>Maria Eisac</h6>
                            <p>Lorem Ipsum dolor</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="anyone-content d-md-none d-block" id="sliders">
            <div class="row">
                <div class="col-md-3 item shows">
                    <div class="over-width">
                        <img src="{{asset('landing/images/anyone-img-1.png')}}" class="img-fluid" alt="">
                        <div class="overlay"></div>
                    </div>
                    <div class="dspn">
                        <h6>Sara James</h6>
                        <p>Lorem Ipsum dolor</p>
                    </div>
                </div>
                <div class="col-md-3 item">
                    <div class="over-width">
                        <img src="{{asset('landing/images/anyone-img-2.png')}}" class="img-fluid" alt="">
                        <div class="overlay"></div>
                    </div>
                    <div class="dspn">
                        <h6>Ann Mathew</h6>
                        <p>Lorem Ipsum dolor</p>
                    </div>
                </div>
                <div class="col-md-3 item">
                    <div class="over-width">
                        <img src="{{asset('landing/images/anyone-img-3.png')}}" class="img-fluid" alt="">
                        <div class="overlay"></div>
                    </div>
                    <div class="dspn">
                        <h6>Maria Eisac</h6>
                        <p>Lorem Ipsum dolor</p>
                    </div>
                </div>
                <div class="col-md-3 item">
                    <div class="over-width">
                        <img src="{{asset('landing/images/anyone-img-4.png')}}" class="img-fluid" alt="">
                        <div class="overlay"></div>
                    </div>
                    <div class="dspn">
                        <h6>Jassic John</h6>
                        <p>Lorem Ipsum dolor</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--<div class="successful-home-businesses">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12">--}}
{{--                <h1>1 Crore+ Resellers Run Successful Home Businesses with Us</h1>--}}
{{--                <p>Watch their journey towards growth & confidence!</p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="container-fluid">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12 pr-md-0 pl-md-0">--}}
{{--                <div class="slider-main">--}}
{{--                    <div class="slider-inner">--}}
{{--                        <a href="" data-toggle="modal" data-target="#exampleModal1-">--}}
{{--                            <div class="slider-contents">--}}
{{--                                <img src="{{asset('landing/images/success-img-1.png')}}" class="img-fluid" alt="">--}}
{{--                                <div class="overlay"></div>--}}
{{--                                <div class="sub-content">--}}
{{--                                    <h5><em> 'I earn ₹10-12,000 per month from home with Bada Business, while--}}
{{--                                            getting--}}
{{--                                            the--}}
{{--                                            flexibility of taking care of my family'</em></h5>--}}
{{--                                    <h6>Jansi Anthony</h6>--}}
{{--                                    <p>Homemaker</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <div class="slider-inner">--}}
{{--                        <a href="" data-toggle="modal" data-target="#exampleModal2-">--}}
{{--                            <div class="slider-contents">--}}
{{--                                <img src="{{asset('landing/images/success-img-2.png')}}" class="img-fluid" alt="">--}}
{{--                                <div class="overlay"></div>--}}
{{--                                <div class="sub-content">--}}
{{--                                    <h5><em> 'I earn ₹10-12,000 per month from home with Bada Business, while--}}
{{--                                            getting--}}
{{--                                            the--}}
{{--                                            flexibility of taking care of my family'</em></h5>--}}
{{--                                    <h6>Jansi Anthony</h6>--}}
{{--                                    <p>Homemaker</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <div class="slider-inner">--}}
{{--                        <a href="" data-toggle="modal" data-target="#exampleModal3-">--}}
{{--                            <div class="slider-contents">--}}
{{--                                <img src="{{asset('landing/images/success-img-1.png')}}" class="img-fluid" alt="">--}}
{{--                                <div class="overlay"></div>--}}
{{--                                <div class="sub-content">--}}
{{--                                    <h5><em> 'I earn ₹10-12,000 per month from home with Bada Business, while--}}
{{--                                            getting--}}
{{--                                            the--}}
{{--                                            flexibility of taking care of my family'</em></h5>--}}
{{--                                    <h6>Jansi Anthony</h6>--}}
{{--                                    <p>Homemaker</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<div class="testimonial-slider">--}}
{{--    <div class="container">--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-11">--}}
{{--                <div class="slider-test">--}}
{{--                    <div class="test-inner">--}}
{{--                        <img src="{{asset('landing/images/Thumb-2.png')}}" class="img-fluid" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="test-inner">--}}
{{--                        <img src="{{asset('landing/images/Thumb-1.png')}}" class="img-fluid" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="test-inner">--}}
{{--                        <img src="{{asset('landing/images/Thumb-3.png')}}" class="img-fluid" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="test-inner">--}}
{{--                        <img src="{{asset('landing/images/Thumb-2.png')}}" class="img-fluid" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="test-inner">--}}
{{--                        <img src="{{asset('landing/images/Thumb-1.png')}}" class="img-fluid" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="test-inner">--}}
{{--                        <img src="{{asset('landing/images/Thumb-3.png')}}" class="img-fluid" alt="">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<div class="in-the-news">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>@lang('website.In the news')</h1>
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('landing/images/economictimes.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('landing/images/yourstory.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('landing/images/timesofindia.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('landing/images/livemint.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('landing/images/businessworld.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('landing/images/ycombinator.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('landing/images/techcrunch.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-3 text-center">
                <img src="{{asset('landing/images/techinasia.png')}}" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</div>
<div class="zero-investment">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>@lang('website.Start Your Business from')<br> @lang('website.Home with Zero Investment')</h1>
                <p>@lang('website.Join Indias #1'), @lang('website.Customer’s Favorite'),<br> MEHAR BADA BUSINESS Platform</p>
                <div class="link-form d-none d-md-block">
                    <form>
                        <div class="std-code">+91</div>
                        <input type="text" class="form-input" name="phone" placeholder="@lang('website.Please enter your Phone Number')">
                        <button type="submit" class="submit-button">Get App Link</button>
                    </form>
                </div>
                <div class="links">
                    <a href="https://play.google.com/store/apps/details?id=com.mehar.meharbadabusiness" target="_blank">
                        <img src="{{asset('landing/images/google-play-btn.png')}}" class="img-fluid" alt="">
                    </a>
                    <a href="">
                        <img src="{{asset('landing/images/play-store-btn.png')}}" class="img-fluid" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-6 text-center">
                <div class="zero-img">
                    <img src="{{asset('landing/images/phone-download-bg.png')}}" class="img-fluid" alt="">
                </div>

            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <a href="index.html">
                    <img src="{{asset('landing/images/footer-logo.svg')}}" class="img-fluid" alt="">
                </a>
                <p>@lang('website.join mehar')</p>
            </div>
            <div class="col-md-3">
                <h4>@lang('website.Company')</h4>
                <ul>
                    <li><a href="">@lang('website.Careers')</a></li>
                    <li><a href="">@lang('website.Bada Business Blog')</a></li>
                    <li><a href="">@lang('website.Mehar Tech Blog')</a></li>
                    <li><a href="">@lang('website.T&C')</a></li>
                    <li><a href="">@lang('website.Privacy')</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <h4>@lang('website.Contact Us')</h4>
                <ul>
                    <li><a href="">1800 572 6161</a></li>
                    <li><a href="">care@meharbadabusiness.com</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="border-bottem"></div>
            </div>
            <div class="col-md-6">
                <div class="social-media">
                    <a href=""><img src="{{asset('landing/images/fb.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/insta.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/tw.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/in.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/YT.png')}}" class="img-fluid" alt=""></a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="copy-right">
                    <p>© 2020-21 meharbadabusiness. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe src="https://www.youtube.com/embed/JRK7RjOB3Bk" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe src="https://www.youtube.com/embed/JRK7RjOB3Bk" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe src="https://www.youtube.com/embed/JRK7RjOB3Bk" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe src="https://www.youtube.com/embed/JRK7RjOB3Bk" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>

            </div>
        </div>
    </div>
</div>


<script src="{{asset('landing/js/jquery-3.4.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('landing/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('landing/js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('landing/js/aos.js')}}" type="text/javascript"></script>
<script src="{{asset('landing/js/slick.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    AOS.init();
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //call the function when ready
        slideShow();


        //Actually define the slideShow()
        function slideShow() {

            //*** Conditional & Variables ***//

            //Define the current img
            var current = $('#slider .show');
            //If index != 0/false then show next img
            var next = current.next().length ?
                current.next() :
                // if index == false then show first img
                current.siblings().first();

            //*** Swap out the imgs and class ***//
            current.hide().removeClass('show');
            next.fadeIn("slow").addClass('show');


            //*** Repeat function every 3 seconds ***//
            setTimeout(slideShow, 3000);

        };

    }); //end ready
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //call the function when ready
        slideShow();


        //Actually define the slideShow()
        function slideShow() {

            //*** Conditional & Variables ***//

            //Define the current img
            var current = $('#sliders .shows');
            //If index != 0/false then show next img
            var next = current.next().length ?
                current.next() :
                // if index == false then show first img
                current.siblings().first();

            //*** Swap out the imgs and class ***//
            current.hide().removeClass('shows');
            next.fadeIn("slow").addClass('shows');


            //*** Repeat function every 3 seconds ***//
            setTimeout(slideShow, 3000);

        };

    }); //end ready
</script>
<script>
    $(document).ready(function () {
        $('.modal').on('hidden.bs.modal', function () {
            var $this = $(this).find('iframe'),
                tempSrc = $this.attr('src');
            $this.attr('src', "");
            $this.attr('src', tempSrc);
        });
    });
</script>
<script>
    $('.slider-test').slick({
        dots: false,
        infinite: true,
        arrows: true,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: false,
        variableWidth: true,
        prevArrow: '<button class="slick-prev slide-chever"> <span></span> </button>',
        nextArrow: '<button class="slick-next slide-chever"> <span></span> </button>',
        responsive: [

            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true,
                }
            }

        ]
    });
</script>

<script>
    $('.slider-main').slick({
        dots: false,
        infinite: false,
        arrows: true,
        speed: 1000,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: false,
        variableWidth: true,
        centerMode: true,
        prevArrow: '<button class="slick-prev slide-arrow"> <span></span> </button>',
        nextArrow: '<button class="slick-next slide-arrow"> <span class="change"></span> </button>',
        responsive: [

            {
                breakpoint: 480,
                settings: "unslick"
            }

        ]
    });

</script>
<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();

        $(".slick-next").click(function () {
            if ($(".slick-next").hasClass("slick-disabled"))
                $(".slider-main .slick-list.draggable").addClass("padding");
        });
        $(".slick-prev").click(function () {
            $(".slider-main .slick-list.draggable").removeClass("padding");
        });
    });

</script>
<script>
    function myFunction1(lang_id) {
        jQuery(function ($) {
            jQuery.ajax({
                beforeSend: function (xhr) { // Add this line
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                },
                url: '{{ URL::to("/change_language")}}',
                type: "POST",
                data: {"languages_id": lang_id, "_token": "{{ csrf_token() }}"},
                success: function (res) {
                    window.location.reload();
                },
            });
        });
    }
</script>










</body>

</html>