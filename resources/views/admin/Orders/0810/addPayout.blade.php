@extends('admin.layout')
@section('content')


    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> {{ trans('labels.AddPayouts') }} <small>{{ trans('labels.AddPayouts') }}...</small></h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i
                                class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
                <li><a href="{{ URL::to('admin/orders/orderstatus')}}"><i
                                class="fa fa-dashboard"></i>{{ trans('labels.ListingOrderStatus') }}</a></li>
                <li class="active">{{ trans('labels.AddOrderStatus') }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->

            <!-- /.row -->

            <div class="row">
                <div class="col-md-12">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{ trans('labels.AddPayouts') }}</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    @if (count($errors) > 0)
                                        @if($errors->any())
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                {{$errors->first()}}
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <!-- form start -->
                                        <div class="box-body">

                                            {!! Form::open(array('url' =>'admin/savepayout', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}

                                            <div class="form-group" hidden>
                                                <label for="name"
                                                       class="col-sm-2 col-md-3 control-label">{{ trans('labels.Status Type') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <select name="role_id" class="form-control">
                                                        <option value="2"
                                                                selected>{{ trans('labels.General') }}</option>
                                                    </select>
                                                    <span class="help-block"
                                                          style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                      {{ trans('labels.StatusLanguageText') }}</span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="name"
                                                       class="col-sm-2 col-md-3 control-label">{{ trans('labels.Select Customer') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    {{--                                                    <select name="public_flag" class="form-control">--}}
                                                    {{--                                                        @foreach($result['users'] as $each)--}}
                                                    {{--                                                            <option value="0"--}}
                                                    {{--                                                                    data-content="{{$each->amounts}}">{{ $each->first_name }} {{ $each->last_name }}</option>--}}
                                                    {{--                                                        @endforeach--}}

                                                    {{--                                                    </select>--}}
                                                    <select class="form-control selectpicker customer_change"
                                                            id="select-customer"
                                                            name="user"
                                                            data-live-search="true">
                                                        <option value="0"
                                                        >Select Customer
                                                        </option>
                                                        @foreach($result['users'] as $each)
                                                            <option value="{{$each->id}}"
                                                                    data-num="{{$each->total_earnings}}"
                                                                    data-title="{{$each->total_approved}}">{{ $each->first_name }} {{ $each->last_name }}
                                                                ({{$each->email}})
                                                            </option>
                                                        @endforeach
                                                    </select>


                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="name"
                                                       class="col-sm-2 col-md-3 control-label">{{ trans('labels.Total Earnings') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <input type="text" name="OrdersStatus_4" id="total_earnings"
                                                           class="form-control field-validate" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name"
                                                       class="col-sm-2 col-md-3 control-label">{{ trans('labels.Approved Earnings') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <input type="text" name="OrdersStatus_4" id="approved_earnings"
                                                           class="form-control field-validate" readonly>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name"
                                                       class="col-sm-2 col-md-3 control-label">{{ trans('labels.Enter Amount') }}</label>
                                                <div class="col-sm-10 col-md-4">
                                                    <input type="number" name="amount" id="amount"
                                                           min="0" max=""
                                                           class="form-control field-validate">
                                                    <span class="help-block ">This field is required.</span>
                                                </div>
                                            </div>


                                            <!-- /.box-body -->
                                            <div class="box-footer text-right">
                                                <div class="col-sm-offset-2 col-md-offset-3 col-sm-10 col-md-4">
                                                    <button type="button"
                                                            class="btn btn-primary loading"
                                                            style="display: none"
                                                            disabled>loading...
                                                    </button>
                                                    <button type="submit"
                                                            class="btn btn-primary submit">{{ trans('labels.Submit') }}</button>
                                                    <a href="payouts" type="button"
                                                       class="btn btn-default">{{ trans('labels.back') }}</a>
                                                </div>
                                            </div>
                                            <!-- /.box-footer -->
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Main row -->

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script>
        $(function () {
            $('.selectpicker').selectpicker();
        });
        $('#select-customer').change(function () {
            var id = $(this).val();
            // var total_earnings = $(this).children("option:selected").attr('data-num');
            // var approved_earnings = $(this).children("option:selected").attr('data-title');
            // $('#total_earnings').val(total_earnings);
            // $('#approved_earnings').val(approved_earnings);
            // $('#amount').val(approved_earnings);
            // $('#amount').attr('max', approved_earnings);
            $('.loading').css('display', '');
            $('.submit').css('display', 'none');
            $.ajax({
                type: 'GET',
                global: false,
                data: {id: id,},
                url: '/admin/getpayout',
                success: function (data) {
                    var total_earnings = data.total_earnings;
                    var approved_earnings = data.total_approved;
                    $('#total_earnings').val(total_earnings);
                    $('#approved_earnings').val(approved_earnings);
                    $('#amount').val(approved_earnings);
                    $('#amount').attr('max', approved_earnings);
                    $('.loading').css('display', 'none');
                    $('.submit').css('display', '');
                }
            });
        });
        $('#amount').change(function () {
            var min = $(this).attr('min');
            var max = $(this).attr('max');
            var val = $(this).val();
            if (min > val) {
                $(this).val(min);
            }
            if (max < val) {
                $(this).val(max);
            }
        })

    </script>

@endsection
