@extends('admin.layout')
<style>
    .wrapper.wrapper2 {
        display: block;
    }

    .wrapper {
        display: none;
    }
</style>
<body onload="window.print();">
<div class="wrapper wrapper2">
    <!-- Main content -->
    <section class="invoice" style="margin: 15px;">
        <!-- title row -->
        <div class="col-xs-12">
            <div class="row">
                @if(session()->has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-check"></i> {{ trans('labels.Successlabel') }}</h4>
                        {{ session()->get('message') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-warning alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-warning"></i> {{ trans('labels.WarningLabel') }}</h4>
                        {{ session()->get('error') }}
                    </div>
                @endif


            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header" style="padding-bottom: 25px">
                    <i class="fa fa-globe"></i> {{ trans('labels.OrderID') }}# {{ $data['orders_data'][0]->orders_id }}
                    <small class="pull-right">{{ trans('labels.OrderedDate') }}
                        : {{ date('m/d/Y', strtotime($data['orders_data'][0]->date_purchased)) }}</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>

        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>{{ trans('labels.Qty') }}</th>
                        <th>Product Image</th>

                        <th>{{ trans('labels.ProductName') }}</th>
                        <th>{{ trans('labels.ProductModal') }}</th>
                        <th>{{ trans('labels.Options') }}</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $commission = 0;?>
                    @foreach($data['orders_data'][0]->data as $products)
                        <?php $commission += $products->commission?>

                        <tr>
                            <td>{{  $products->products_quantity }}</td>
                            <td>
                                <img src="{{  Storage::disk('s3')->url($products->image) }}" width="300px">


                            </td>
                            <td width="30%">
                                {{  $products->products_name }}<br>
                            </td>
                            <td></td>
                            <td>
                                @foreach($products->attribute as $attributes)
                                    <b>{{ trans('labels.Name') }}:</b> {{ $attributes->products_options }}<br>
                                    <b>{{ trans('labels.Value') }}:</b> {{ $attributes->products_options_values }}<br>
                                    <b>{{ trans('labels.Price') }}
                                        :</b> @if(!empty($result['commonContent']['currency']->symbol_left)) {{ $attributes->options_values_price }} @endif {{ $data['orders_data'][0]->shipping_cost }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif
                                    <br>

                                @endforeach</td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>

