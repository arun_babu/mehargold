@extends('admin.layout')
@section('content')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> {{ trans('labels.Payouts') }} <small>Customer Payouts...</small></h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard/this_month') }}"><i
                                class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
                <li class="active">Customer Payouts</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->

            <!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Customer Payouts </h3>
                            <div class="box-tools pull-right">

                            </div>
                        </div>

                        <!-- /.box-header -->

                        <div class="box-body">

                            <div class="row" style="margin: 10px">
                                <label for="name"
                                       class="col-sm-2 col-md-3 control-label">{{ trans('labels.Select Customer') }}</label>
                                <form action="" method="get">
                                <div class="col-xs-4 m-3 ">
                                    <div class="input-group-form search-panel ">
                                        <select class="form-control selectpicker "
                                                id="select-customer"
                                                name="user"
                                                data-live-search="true">
                                            <option value="0">All</option>
                                            @foreach ($users as $key=>$orderData)
                                                <option value="{{$orderData->id}}"
                                                        @if($filter==$orderData->id)
                                                            selected
                                                            @endif
                                                >{{ $orderData->first_name }} {{ $orderData->last_name }}
                                                    ({{$orderData->email}})
                                                </option>
                                            @endforeach
                                        </select>


                                    </div>
                                </div>
                                <div class="col-xs-2 m-3 ">
                                    <div class="input-group-form search-panel ">
                                        <button class="btn btn-primary " id="submit" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                        @if(isset($filter)) <a class="btn btn-danger " href="{{url('admin/customerPayouts')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                    </div>
                                </div>
                                </form>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    @if (count($errors) > 0)
                                        @if($errors->any())
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                {{$errors->first()}}
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>{{ trans('labels.CustomerName') }}</th>
                                            <th>Total Earnings</th>
                                            <th>Approved Earnings</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($listingOrders['users'])>0)
                                            <?php $count = 1;?>
                                            @foreach ($listingOrders['users'] as $key=>$orderData)
                                                <tr class="all-visible" data-title="{{$orderData->id}}">
                                                    <td class="">{{ $orderData->id }}</td>
                                                    <td>{{ $orderData->first_name }} {{ $orderData->last_name }}</td>
                                                    <td>{{ $orderData->total_earnings }}</td>
                                                    <td>{{ $orderData->total_approved }}</td>
                                                    <td><a data-toggle="tooltip" data-placement="bottom" title=""
                                                           href="/admin/addpayout/{{$orderData->id}}"
                                                           class="badge bg-light-blue"
                                                           data-original-title="Edit"><i
                                                                    class="fa fa-pencil-square-o"
                                                                    aria-hidden="true"></i></a></td>


                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6"><strong>{{ trans('labels.NoRecordFound') }}</strong>
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="col-xs-12 text-right">
                                        {{$listingOrders['users']->links()}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- deleteModal -->
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="deleteModalLabel">{{ trans('labels.DeleteOrder') }}</h4>
                        </div>
                        {!! Form::open(array('url' =>'admin/orders/deleteOrder', 'name'=>'deleteOrder', 'id'=>'deleteOrder', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                        {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                        {!! Form::hidden('orders_id',  '', array('class'=>'form-control', 'id'=>'orders_id')) !!}
                        <div class="modal-body">
                            <p>{{ trans('labels.DeleteOrderText') }}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">{{ trans('labels.Close') }}</button>
                            <button type="submit" class="btn btn-primary"
                                    id="deleteOrder">{{ trans('labels.Delete') }}</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <!-- Main row -->

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script>
        $('.customer_change').on('change', function () {
            var selected = $(this).val();
            $('.all-visible').css('display', '');
            if (selected != 0)
                $('.all-visible').each(function () {
                    console.log($(this).attr('data-title'));
                    if ($(this).attr('data-title') != selected)
                        $(this).css('display', 'none')
                })


        })
    </script>
@endsection
