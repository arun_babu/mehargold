@extends('admin.layout')
<style>
    .wrapper.wrapper2 {
        display: block;
    }

    .wrapper {
        display: none;
    }
</style>
<body onload="window.print();">
<div class="wrapper wrapper2">
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-6">
                <h2 class="page-header" style="padding-bottom: 25px">
{{--                    <i class="fa fa-globe"></i> {{ trans('labels.OrderID') }}# {{ $data['orders_data'][0]->orders_id }}--}}
                    <img src="{{asset('landing/images/mehar-logo-png.png')}}" class="img-fluid" alt="" height="80">
                </h2>



                    {{--                    <small class="pull-right">{{ trans('labels.OrderedDate') }}--}}
{{--                        : {{ date('m/d/Y', strtotime($data['orders_data'][0]->date_purchased)) }}</small>--}}
            </div>
            <div class="col-xs-6 pull-right invoice-info" style="text-align: right;">
                <strong>Retail/Tax Invoice/Cash Memorandum<br></strong>
                (Original for Recipient)

            </div>

            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <strong>Sold By:</strong><br>
                <?php $iskerala = $data['orders_data'][0]->delivery_city == "Kerala" || $data['orders_data'][0]->delivery_state == "Kerala" || $data['orders_data'][0]->delivery_suburb == "Kerala" ? true : false;?>
                <address>
                    Mehar Gem & Jewellery Pvt.Ltd<br>
                    Trivandrum. Kerala<br>
                    Pin 695541

                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-6 invoice-col pull-right" style="text-align: right;">
                <strong>Billing Address:</strong>
                <address>
                    {{ $data['orders_data'][0]->billing_name }}<br>
                    {{ trans('labels.Phone') }}: {{ $data['orders_data'][0]->billing_phone }}<br>
                    {{ $data['orders_data'][0]->billing_street_address }} <br>
                    {{ $data['orders_data'][0]->billing_city }}<br>
                    {{--                    , {{ $data['orders_data'][0]->billing_state }} --}}
                    Pin:{{ $data['orders_data'][0]->billing_postcode }}
                    , {{ $data['orders_data'][0]->billing_country }}<br>
                </address>
            </div>
        </div>

        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <strong>PAN No:</strong><br>
                <strong>VAT Registration No:</strong><br>
                <strong>CST Registration No:</strong><br>

            </div>
            <!-- /.col -->
            <div class="col-sm-6 invoice-col pull-right" style="text-align: right;">
                <strong>Shipping Address:</strong>
                <?php $iskerala = $data['orders_data'][0]->delivery_city == "Kerala" || $data['orders_data'][0]->delivery_state == "Kerala" || $data['orders_data'][0]->delivery_suburb == "Kerala" ? true : false;?>
                <address>
                    {{ $data['orders_data'][0]->delivery_name }}<br>
                    {{ trans('labels.Phone') }}: {{ $data['orders_data'][0]->delivery_phone }}<br>
                    {{ $data['orders_data'][0]->delivery_street_address }} <br>
                    {{ $data['orders_data'][0]->delivery_city }}<br>
                    Pin:{{ $data['orders_data'][0]->delivery_postcode }}
                    , {{ $data['orders_data'][0]->delivery_country }}<br>
                    {{ trans('labels.Phone') }}: {{ $customerData->phone }}<br>
                </address>
            </div>
        </div>


        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <strong>Order Number: </strong>{{ $data['orders_data'][0]->orders_id }}<br>
                <strong>Order Date: </strong>{{ date('m/d/Y', strtotime($data['orders_data'][0]->date_purchased)) }}<br>
            </div>
            <!-- /.col -->
            <div class="col-sm-6 invoice-col pull-right" style="text-align: right;">
                <strong>Invoice Number:</strong><br>
                <strong>Invoice Date:</strong><br>
            </div>
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row" style="padding-top: 5px">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>{{ trans('labels.Qty') }}</th>
                        <th>Product Code</th>
                        <th>{{ trans('labels.ProductName') }}</th>
                        <th>{{ trans('labels.ProductModal') }}</th>
                        <th>{{ trans('labels.Options') }}</th>
                        <th>{{ trans('labels.Price') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $commission = 0;?>
                    @foreach($data['orders_data'][0]->data as $products)
                        <?php $commission += $products->commission?>

                        <tr>
                            <td>{{  $products->products_quantity }}</td>
                            <td>
                                {{ $products->product_code }}
                            </td>
                            <td width="30%">
                                {{  $products->products_name }}<br>
                            </td>
                            <td></td>
                            <td>
                                @foreach($products->attribute as $attributes)
                                    <b>{{ trans('labels.Name') }}:</b> {{ $attributes->products_options }}<br>
                                    <b>{{ trans('labels.Value') }}:</b> {{ $attributes->products_options_values }}<br>
                                    <b>{{ trans('labels.Price') }}
                                        :</b> @if(!empty($result['commonContent']['currency']->symbol_left)) {{ $attributes->options_values_price }} @endif {{ $data['orders_data'][0]->shipping_cost }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif
                                    <br>

                                @endforeach</td>

                            <td>@if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $products->final_price+$products->commission }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-7">
            </div>
            <!-- /.col -->
            <div class="col-xs-5">
                <!--<p class="lead"></p>-->

                <div class="table-responsive ">
                    <table class="table order-table">
                        <tr>
                            <th style="width:50%">{{ trans('labels.Subtotal') }}:</th>
                            <td>
                                @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['subtotal']+$commission }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif
                            </td>
                        </tr>
                        @if($iskerala)
                            <tr>
                                <th>{{ trans('labels.Tax') }}(SGST 3%):

                                </th>
                                <td>
                                    @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->total_tax }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif

                                </td>
                            </tr>
                        @else
                            <tr>
                                <th>{{ trans('labels.Tax') }}(CGST 1.5%):
                                </th>
                                <td>
                                    @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->total_tax/2 }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif

                                </td>
                            </tr>
                            <tr>
                                <th>{{ trans('labels.Tax') }}(SGST 1.5%):
                                </th>
                                <td>
                                    @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->total_tax/2 }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif

                                </td>
                            </tr>
                        @endif

                        <tr>
                            <th>Discount:</th>
                            <td>
                                @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif
                                0 @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif
                            </td>
                        </tr>
                        <tr>
                            <th>{{ trans('labels.ShippingCost') }}:</th>
                            <td>
                                @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->shipping_cost }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}}@endif
                            </td>
                        </tr>
                        @if(!empty($data['orders_data'][0]->coupon_code))
                            <tr>
                                <th>{{ trans('labels.DicountCoupon') }}:</th>
                                <td>
                                    @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->coupon_amount }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif</td>
                            </tr>
                        @endif
                        <tr>
                            <th>{{ trans('labels.Total') }}:</th>
                            <td>
                                @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->order_price }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif</td>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>


            <!-- /.col -->
        </div>
        <div class="row" style="border: 3px solid #000000;margin: 3px; text-align: right;" >
            <div class="col-xs-12" >
                <strong>For Mehar Gem & Jewellery</strong><br>
                <strong>Private Limited:</strong><br>
                <img src="{{asset('landing/images/signature.jpeg')}}" class="img-fluid" alt="" height="50"><br>
                <strong>Authorized Signatory</strong>
            </div>
        </div>
        <!-- /.row -->


    </section>
    <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>

