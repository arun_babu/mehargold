@extends('admin.layout')
<style>
    .wrapper.wrapper2 {
        display: block;
    }

    .wrapper {
        display: none;
    }

    .custom-table {

    }

    table, th, td {
        border: 1px solid black;
    }
</style>
<body onload="window.print();">
<div class="wrapper wrapper2">
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-6">
                <h2 class="page-header" style="margin-bottom: 0px">
                    {{--                    <i class="fa fa-globe"></i> {{ trans('labels.OrderID') }}# {{ $data['orders_data'][0]->orders_id }}--}}
                    <img src="{{asset('landing/images/mehar-logo-png.png')}}" class="img-fluid" alt="" height="80">
                </h2>


                {{--                    <small class="pull-right">{{ trans('labels.OrderedDate') }}--}}
                {{--                        : {{ date('m/d/Y', strtotime($data['orders_data'][0]->date_purchased)) }}</small>--}}
            </div>
            <div class="col-xs-6 pull-right invoice-info" style="text-align: right;">
                <strong>Retail/Tax Invoice/Cash Memorandum<br></strong>
                (Original for Recipient)

            </div>

            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-8 col-xs-8 invoice-col">
                <strong>Sold By:</strong><br>
                <?php $iskerala = $data['orders_data'][0]->delivery_city == "Kerala" || $data['orders_data'][0]->delivery_state == "Kerala" || $data['orders_data'][0]->delivery_suburb == "Kerala" ? true : false;?>
                <address>
                    Mehar Gem & Jewellery Pvt.Ltd<br>
                    Trivandrum. Kerala<br>
                    Pin 695541<br>
                    <img src="{{asset('landing/images/bill-tollfree.jpeg')}}" class="img-fluid" alt="" height="20"> : 1800 572 6161<br>
                    <img src="{{asset('landing/images/bill-mail.jpeg')}}" class="img-fluid" alt="" height="13" style="margin-left: -2px; margin-right:2px ">  : care@meharbadabusiness.com<br>

                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 col-xs-4 invoice-col pull-right" style="text-align: right;">
                <strong>Billing Address:</strong>
                <address>
                    {{ $data['orders_data'][0]->billing_name }}<br>
                    {{ $data['orders_data'][0]->billing_street_address }} <br>
                    {{ $data['orders_data'][0]->billing_city }}<br>
                    {{--                    , {{ $data['orders_data'][0]->billing_state }} --}}
                    Pin:{{ $data['orders_data'][0]->billing_postcode }}
                    , {{ $data['orders_data'][0]->billing_country }}<br>
                </address>
            </div>
        </div>

        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <strong>GST No:</strong>32AAKCM9058K1Z0<br>
            </div>
            <!-- /.col -->
            <div class="col-sm-6 invoice-col pull-right" style="text-align: right;">
                <strong>Shipping Address:</strong>
                <?php $iskerala = $data['orders_data'][0]->delivery_city == "Kerala" || $data['orders_data'][0]->delivery_state == "Kerala" || $data['orders_data'][0]->delivery_suburb == "Kerala" ? false : true;?>
                <address>
                    {{ $data['orders_data'][0]->delivery_name }}<br>
                    {{ $data['orders_data'][0]->delivery_street_address }} <br>
                    {{ $data['orders_data'][0]->delivery_city }}<br>
                    Pin:{{ $data['orders_data'][0]->delivery_postcode }}
                    , {{ $data['orders_data'][0]->delivery_country }}<br>
                    {{ trans('labels.Phone') }}: {{ $data['orders_data'][0]->delivery_phone }}<br>
                </address>
            </div>
        </div>


        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <strong>Order Number: </strong>{{ $data['orders_data'][0]->orders_id }}<br>
                <strong>Order Date: </strong>{{ date('m/d/Y', strtotime($data['orders_data'][0]->date_purchased)) }}<br>
            </div>
            <!-- /.col -->
            <div class="col-sm-6 invoice-col pull-right" style="text-align: right;">
                <strong>Invoice Number:</strong>{{ $data['orders_data'][0]->orders_id }}<br>
                <strong>Invoice Date:</strong>{{ date('m/d/Y', strtotime($data['orders_data'][0]->date_purchased)) }}<br>
            </div>
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row" style="padding-top: 5px">
            <div class="col-xs-12 table-responsive custom-table">
                <table class="table table-striped" style="border: 1px solid black;">
                    <thead style="border: 1px solid black;">
                    <tr>
                        <th >SL. No</th>
                        {{--                        <th>Product Code</th>--}}
                        <th>HSN code</th>
                        <th>{{ trans('labels.ProductName') }}</th>
                        <th>Price</th>
                        <th>{{ trans('labels.Qty') }}</th>
                        <th>Net Amount</th>
                        <th>Tax Rate</th>
                        <th>Tax Type</th>
                        <th>Tax Amount</th>
                        <th>{{ trans('labels.Price') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $commission = 0;$count = 1;?>
                    @foreach($data['orders_data'][0]->data as $products)
                        <?php $commission += $products->commission;
                        $taxeach = (($products->final_price + $products->commission) * 3) / 100;
                        $taxeach1 = (($products->final_price + $products->commission) * 1.5) / 100;
                        $taxeach2 = (($products->final_price + $products->commission) * 1.5) / 100;
                        ?>
                        <tr>
                            <td rowspan="{{$iskerala?'1':'2'}}">{{  $count++ }}</td>
                            <td rowspan="{{$iskerala?'1':'2'}}">7117</td>
                            <td width="30%" rowspan="{{$iskerala?'1':'2'}}">
                                {{  $products->products_name }}<br>
                                @foreach($products->attribute as $attributes)
                                    <b>{{ trans('labels.Name') }}:</b> {{ $attributes->products_options }}<br>
                                    <b>{{ trans('labels.Value') }}:</b> {{ $attributes->products_options_values }}<br>
                                    <b>{{ trans('labels.Price') }}
                                        :</b> @if(!empty($result['commonContent']['currency']->symbol_left)) {{ $attributes->options_values_price }} @endif @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif
                                    <br>
                                @endforeach
                            </td>
                            <td rowspan="{{$iskerala?'1':'2'}}">{{($products->final_price+$products->commission)/$products->products_quantity}}</td>
                            <td rowspan="{{$iskerala?'1':'2'}}">{{  $products->products_quantity }}</td>
                            <td rowspan="{{$iskerala?'1':'2'}}">{{$products->final_price+$products->commission}}</td>
                            <td>{{$iskerala?'3%':'1.5%'}}</td>
                            <td>{{$iskerala?'IGST':'CGST'}}</td>
                            <td>{{$iskerala?$taxeach:$taxeach1}}</td>
                            <td rowspan="{{$iskerala?'1':'2'}}" width="15%">@if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{$products->final_price+$products->commission+$taxeach}} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif</td>
                        </tr>
                        @if(!$iskerala)
                            <tr>
                                <td>1.5%</td>
                                <td>SGST</td>
                                <td>{{$taxeach2}}</td>
                            </tr>
                        @endif

                    @endforeach
                    <tr>
                        <td >{{  $count++ }}</td>
                        <td width="8%"></td>
                        <td width="30%">Shipping Charges</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td >{{($data['orders_data'][0]->shipping_cost)}}</td>
                    </tr>
                    <tr>
                        <td colspan="9">Total</td>
                        <td>
                            @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->order_price }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

{{--        <div class="row">--}}
{{--            <!-- accepted payments column -->--}}
{{--            <div class="col-xs-7">--}}
{{--            </div>--}}
{{--            <!-- /.col -->--}}
{{--            <div class="col-xs-5">--}}
{{--                <!--<p class="lead"></p>-->--}}

{{--                <div class="table-responsive ">--}}
{{--                    <table class="table order-table">--}}
{{--                        <tr>--}}
{{--                            <th style="width:50%">{{ trans('labels.Subtotal') }}:</th>--}}
{{--                            <td>--}}
{{--                                @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['subtotal']+$commission }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                        @if($iskerala)--}}
{{--                            <tr>--}}
{{--                                <th>{{ trans('labels.Tax') }}(SGST 3%):--}}

{{--                                </th>--}}
{{--                                <td>--}}
{{--                                    @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->total_tax }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif--}}

{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        @else--}}
{{--                            <tr>--}}
{{--                                <th>{{ trans('labels.Tax') }}(CGST 1.5%):--}}
{{--                                </th>--}}
{{--                                <td>--}}
{{--                                    @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->total_tax/2 }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif--}}

{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <th>{{ trans('labels.Tax') }}(SGST 1.5%):--}}
{{--                                </th>--}}
{{--                                <td>--}}
{{--                                    @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->total_tax/2 }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif--}}

{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        @endif--}}

{{--                        <tr>--}}
{{--                            <th>Discount:</th>--}}
{{--                            <td>--}}
{{--                                @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif--}}
{{--                                0 @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <th>{{ trans('labels.ShippingCost') }}:</th>--}}
{{--                            <td>--}}
{{--                                @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->shipping_cost }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}}@endif--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                        @if(!empty($data['orders_data'][0]->coupon_code))--}}
{{--                            <tr>--}}
{{--                                <th>{{ trans('labels.DicountCoupon') }}:</th>--}}
{{--                                <td>--}}
{{--                                    @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->coupon_amount }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif</td>--}}
{{--                            </tr>--}}
{{--                        @endif--}}
{{--                        <tr>--}}
{{--                            <th>{{ trans('labels.Total') }}:</th>--}}
{{--                            <td>--}}
{{--                                @if(!empty($result['commonContent']['currency']->symbol_left)) {{$result['commonContent']['currency']->symbol_left}} @endif {{ $data['orders_data'][0]->order_price }} @if(!empty($result['commonContent']['currency']->symbol_right)) {{$result['commonContent']['currency']->symbol_right}} @endif</td>--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                    </table>--}}
{{--                </div>--}}

{{--            </div>--}}


{{--            <!-- /.col -->--}}
{{--        </div>--}}
        <div class="row" style="border: 3px solid #000000;margin: 3px; ">
            <div class="col-xs-8" style="text-align: left; padding-top: 10px !important;">
                <strong>Declaration</strong><br>
                Certified that all the particulars shown in the above tax invoice are true and correct in all respect and the goods on which the tax charged collected are in accordance with the provision of the GST Act.

            </div>
            <div class="col-xs-4" style="text-align: right;">
                <strong>For Mehar Gem & Jewellery</strong><br>
                <strong>Private Limited:</strong><br>
                <img src="{{asset('landing/images/signature.jpeg')}}" class="img-fluid" alt="" height="50"><br>
                <strong>Authorized Signatory</strong>
            </div>
        </div>
        <!-- /.row -->


    </section>
    <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>

