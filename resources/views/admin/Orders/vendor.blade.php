@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> Vendor Orders <small>Vendor Orders...</small></h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard/this_month') }}"><i
                                class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
                <li class="active">Vendor Orders</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Vendor Orders </h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row" style="margin: 10px">
                                <form action="{{url('admin/orders/vendorOrders')}}" method="get">
                                    <div class="col-xs-2 m-3">
                                        <input class="form-control datepicker" readonly
                                               type="text" name="start_date"
                                               id="start_date" readonly value="{{$listingOrders['start_date']}}">
                                    </div>
                                    <div class="col-xs-2 m-3">
                                        <input class="form-control datepicker" readonly
                                               type="text" name="end_date"
                                               id="start_date" readonly value="{{$listingOrders['end_date']}}">

                                    </div>
                                    <div class="col-xs-4 m-3">
                                        <select class="form-control" name="vendor" id="filter">
                                            <option value="0">All</option>
                                            @foreach ($listingOrders['manufacturers'] as $key=>$manufacturers)
                                                <option value="{{$manufacturers->manufacturers_id}}" {{$manufacturers->manufacturers_id==$listingOrders['vendor']?'selected':''}}>{{ $manufacturers->manufacturer_name}} </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-xs-2 m-3">
                                        <button class="btn btn-primary " id="submit" type="submit"><span
                                                    class="glyphicon glyphicon-search"></span></button>

                                        @if($listingOrders['filter']) <a class="btn btn-danger "
                                                                         href="{{url('admin/orders/vendorOrders')}}"><i
                                                    class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                    </div>
                                    <div class="col-xs-2 m-3">
                                        <button class="btn btn-primary " id="cmd" type="button"><span
                                                    class="fa fa-print"></span></button>

                                    </div>
                                </form>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    @if (count($errors) > 0)
                                        @if($errors->any())
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                {{$errors->first()}}
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="row " id="content" >
                                <div class="col-xs-12">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            {{--                                            <th>{{ trans('labels.ID') }}</th>--}}
                                            <th>Image</th>
                                            <th>Product Name</th>
                                            <th>Product Attribute</th>
                                            {{--                                            <th>Product Colour</th>--}}
                                            {{--                                            <th>Product Size</th>--}}
                                            <th>Quantity</th>
                                            <th width="20px">Order Number</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($listingOrders['orders'])>0)
                                            @foreach ($listingOrders['orders'] as $key=>$orderData)
                                                <tr>
                                                    {{--                                                    <td>{{ $orderData->orders_id }}</td>--}}
                                                    <td>
                                                        <img src="{{Storage::disk('s3')->url($orderData->default_image?$orderData->default_image:$orderData->image)}}"
                                                             width="60px">
                                                    </td>
                                                    <td>
                                                        {{$orderData->products_name}}
                                                    </td>
                                                    <td>
                                                        @foreach($orderData->attribute as $attributes)
                                                            <b>{{ trans('labels.Name') }}
                                                                :</b> {{ $attributes->products_options }}<br>
                                                            <b>{{ trans('labels.Value') }}
                                                                :</b> {{ $attributes->products_options_values }}
                                                            <br>
                                                        @endforeach
                                                    </td>
                                                    <td>{{$orderData->quantity}}</td>
                                                    <td width="5px" style=" word-break: break-all;
">
                                                        {{$orderData->orders_id}}
                                                    </td>


                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6"><strong>{{ trans('labels.NoRecordFound') }}</strong>
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="col-xs-12 text-right">
{{--                                        {{$listingOrders['orders']->links()}}--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Main row -->

            <!-- /.row -->
        </section>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
        <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

        <script>
            var doc = new jsPDF();
            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };

            $('#cmd').click(function () {
                // var divContents = document.getElementById("content").innerHTML;
                // var a = window.open();
                // a.document.write('<html>');
                // a.document.write('<body > <h1>Div contents are <br>');
                // a.document.write(divContents);
                // a.document.write('</body></html>');
                // a.document.close();
                // a.print();
                var printContents = document.getElementById('content').innerHTML;
                var originalContents = document.body.innerHTML;

                document.body.innerHTML = printContents;

                window.print();
                //
                document.body.innerHTML = originalContents;
                // -------------
                // doc.fromHTML($('#content').html(), 15, 15, {
                //     'width': 170,
                //     'elementHandlers': specialElementHandlers
                // },function() {
                //     doc.save('sample-file.pdf');
                // });
                // doc.save('sample-file.pdf');
            });
        </script>
        <!-- /.content -->
    </div>
@endsection
