<style>
    .wrapper.wrapper2 {
        display: block;
    }

    .wrapper {
        display: none;
    }
</style>
<body onload="window.print();" style="border: 3px solid #000000; height: 50%">
<div class="wrapper wrapper2">
    <!-- Main content -->
    <div class="invoice" style="margin: 15px;">
        <div class="row p-5" style="height: 10%">
            <div class="col-12" style="float: right">
                <img src="{{asset('landing/images/mehar-logo-png.png')}}" class="img-fluid" alt="" height="80">

            </div>
        </div>
        <br>

        <div class="row p-5" style="height: 20%">
            <div class="col-12">
                <h3>
                    @if($data['orders_data'][0]->payment_method=='Cash on Delivery')

                        <b>SPEED PARCEL WITH COD BILLER ID:............<br></b>
                        <b>COD RS:............<br></b>
                    @endif

                </h3>
            </div>
        </div>
        <div class="row p-5" style="height: 40%">
            <div class="col-md-6 col-6 col-sm-6 col-lg-6 col-xs-6" style="width: 50%;float: left;">
                <h3>
                    <b>From<br></b>
                    <b style="padding-left: 20px">Mehar Gem & Jewellery Pvt.Ltd</b><br>
                    <b style="padding-left: 20px">Trivandrum. Kerala</b><br>
                    <b style="padding-left: 20px">Pin 695541</b><br>


                </h3>
            </div>
            <div class="col-md-6 col-6 col-sm-6 col-lg-6 col-xs-6" style="width: 50%;float: right">
                <h3>
                    <b>To<br></b>

                    <b style="padding-left: 20px">{{ $data['orders_data'][0]->delivery_name }}</b><br>
                    <b style="padding-left: 20px">{{ $data['orders_data'][0]->delivery_street_address }}</b><br>
                    <b style="padding-left: 20px">{{ $data['orders_data'][0]->delivery_city }}</b><br>
                    <b style="padding-left: 20px">Pin:{{ $data['orders_data'][0]->delivery_postcode }}</b><br>
                    <b style="padding-left: 20px">{{ $data['orders_data'][0]->delivery_country }}</b><br>
                    <b style="padding-left: 20px">Phone:{{ $data['orders_data'][0]->delivery_phone }}</b><br>

                </h3>
            </div>
        </div>
        <div class="row p-5" style="height: 10%">
            <h3>

                <b><img src="{{asset('landing/images/bill-call.jpeg')}}" class="img-fluid" alt="" height="13" style="margin-left: -3px; margin-right:11px ">: 1800 572 6161<br></b>
                <b><img src="{{asset('landing/images/bill-tollfree.jpeg')}}" class="img-fluid" alt="" height="20" style="margin-left: -6px;margin-right: 3px;"> : 7012248711<br></b>
                <b><img src="{{asset('landing/images/bill-mail.jpeg')}}" class="img-fluid" alt="" height="15" style="margin-left: -11px;margin-right: 5px;"> : care@meharbadabusiness.com<br></b>
            </h3>
        </div>

    </div>
    <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>

