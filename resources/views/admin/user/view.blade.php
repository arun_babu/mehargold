@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> {{ trans('labels.ViewUser') }} <small> {{ trans('labels.ViewUser') }}...</small></h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i
                                class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
                <li><a href="{{ URL::to('admin/usermanagement')}}"><i
                                class="fa fa-dashboard"></i> {{ trans('labels.ViewAllUser') }}</a></li>
                <li class="active"> {{ trans('labels.ViewUser') }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="invoice" style="margin: 15px;">
            <!-- title row -->
            @if(session()->has('message'))
                <div class="col-xs-12">
                    <div class="row">
                        <div class="alert alert-success alert-dismissible">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <h4><i class="icon fa fa-check"></i> {{ trans('labels.Successlabel') }}</h4>
                            {{ session()->get('message') }}
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-xs-12">

                </div>
                <!-- /.col -->
            </div>


            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">

                        <tbody>
                        <tr>
                            <td><b>Full Name</b></td>
                            <td>{{$user->first_name}} {{$user->last_name}}</td>
                        </tr>
                        <tr>
                            <td><b>Email</b></td>
                            <td>{{$user->email}} </td>
                        </tr>
                        <tr>
                            <td><b>Phone</b></td>
                            <td>{{$user->phone}} </td>
                        </tr>
                        <tr>
                            <td><b>Gender</b></td>
                            <td>{{$user->gender?'Male':'Female'}} </td>
                        </tr>
                        <tr>
                            <td><b>Address</b></td>
                            <td>{{$user->address}}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Country</b></td>
                            <td>{{$user->country}}
                            </td>
                        </tr>
                        <tr>
                            <td><b>State</b></td>
                            <td>{{$user->state}}
                            </td>
                        </tr>
                        <tr>
                            <td><b>District</b></td>
                            <td>{{$user->district}}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Taluk</b></td>
                            <td>{{$user->taluk}}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Document</b></td>
                            {{--                            <td>{{$user->doc}}</td>--}}

                            <td>
                                @if($user->doc=='Link')
                                    <a data-toggle="modal">No document found.</a>
                                @else
                                    <a data-toggle="modal" data-target="#modalPreview">{{$user->doc}}</a>
                                @endif
                            </td>
                        </tr>


                        </tbody>
                    </table>
                    <table class="table order-table">
                        <thead>
                        <tr class="d-flex">
                            {{--                            <th class="col-12 col-md-2">@lang('website.ID')</th>--}}
                            <th class="col-12 col-md-3">@lang('website.Bank Name')</th>
                            <th class="col-12 col-md-3">@lang('website.Account Number')</th>
                            <th class="col-12 col-md-3">@lang('website.IFSC Code')</th>
                            <th class="col-12 col-md-3">@lang('website.Phone Number')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($bankdetais))
                            @foreach( $bankdetais as $each)
                                <tr class="d-flex">
                                    {{--                                    <td class="col-12 col-md-2">{{$each->id}}</td>--}}
                                    <td class="col-12 col-md-3">
                                        {{$each->bank_name}}
                                    </td>
                                    <td class="col-12 col-md-3">
                                        {{$each->account_number}}
                                    </td>
                                    <td class="col-12 col-md-3">
                                        {{$each->ifsc_code}}
                                    </td>
                                    <td class="col-12 col-md-3">
                                        {{$each->phone_number}}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">
                                    no details found.
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="modal" tabindex="-1" role="dialog" id="modalPreview">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">CV Preview</h5>
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-label="Close" style="margin-top: -20px">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form-cv-create" method="post">

                                <div class="modal-body">
                                    <iframe width="100%" height="600" name="plugin"
                                            src="{{Storage::disk('s3')->url($user->doc_link)}}"


                                            type="application/pdf"
                                    >
                                    </iframe>

                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <!-- /.col -->
                {!! Form::open(array('url' =>'admin/usermanagement/status', 'method'=>'post', 'onSubmit'=>'return cancelOrder();', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                <input name="id" value="{{$user->id}}" type="hidden">
                <div class="col-xs-12">
                    <hr>
                    <p class="lead">Membership Status:</p>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Status:</label>
                            <select class="form-control select2" id="" name="status" style="width: 100%;">
                                <option value="2" {{$user->status==2?'selected':''}}>Pending</option>
                                <option value="3" {{$user->status==3?'selected':''}}>In progress</option>
                                <option value="1"{{$user->status==1?'selected':''}}>Approve</option>
                                <option value="4" {{$user->status==4?'selected':''}}>Reject</option>

                            </select>
                            <span class="help-block"
                                  style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.ChooseStatus') }}</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{ trans('labels.Comments') }}:</label>
                            {!! Form::textarea('comments',  $user->status_description, array('class'=>'form-control', 'id'=>'comments', 'rows'=>'4'))!!}
                            <span class="help-block"
                                  style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.CommentsOrderText') }}</span>
                        </div>
                    </div>
                </div>
                <!-- this row will not appear when printing -->
                <div class="col-xs-12">
                    <a href="{{ URL::to('admin/usermanagement')}}" class="btn btn-default"><i
                                class="fa fa-angle-left"></i> {{ trans('labels.back') }}</a>
                    <button type="submit" class="btn btn-success pull-right"><i
                                class="fa fa-credit-card"></i> {{ trans('labels.Submit') }} </button>
                    <!--<button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                      <i class="fa fa-download"></i> Generate PDF
                    </button>-->

                    <br><br>
                    <hr>
                    <br>

                </div>
            {!! Form::close() !!}

            <!-- /.col -->
            </div>
            <!-- /.row -->


        </section>
        <!-- /.content -->
    </div>
@endsection
