@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> {{ trans('labels.ListingAllAds') }} <small>{{ trans('labels.ListingAllAds') }}...</small></h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard/this_month') }}"><i
                                class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
                <li class="active">{{ trans('labels.ViewAllAds') }}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">

                        <div class="box-header">
                            <h3 class="box-title">{{ trans('labels.ListingAllAds') }} </h3>
                            <div class="box-tools pull-right">
                                <a href="insertad" type="button"
                                   class="btn btn-block btn-primary">Add New</a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    @if (count($errors) > 0)
                                        @if($errors->any())
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                {{$errors->first()}}
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Image</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($result['ad'])>0)
                                            <?php $count = 1;?>
                                            @foreach ($result['ad'] as $key=>$ad)
                                                <tr class="visible-{{$ad->id}} all-visible">
                                                    <td class="slno">{{ $count++ }}</td>
                                                    <td class="slno">{{ $ad->name }} </td>
                                                    <td class="slno">{{$ad->description}}</td>
                                                    <td class="slno">
                                                        <img class="img-fluid"
                                                             src="{{Storage::disk('s3')->url($ad->image)}}"
                                                             width="50" alt="Banner Image">

                                                    </td>
                                                    <td>
                                                        @if($ad->active==0)
                                                            <span class="label label-warning">
                                                                inactive
                                                        @elseif($ad->active==1)
                                                                    <span class="label label-success">
                                                                        Active
                                                        @endif

                                                            </span>
                                                    </td>
                                                    <td class="slno"><a href="adEdit/{{$ad->id}}"><i class="fa fa-edit"
                                                                                                 aria-hidden="true"></i></a>
                                                        @if($ad->active!=1)
                                                        <a href="adDelete/{{$ad->id}}"  style="margin-left: 5px"><i class="fa fa-trash"
                                                                                        aria-hidden="true"></i></a>
                                                            @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6"><strong>{{ trans('labels.NoRecordFound') }}</strong>
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="col-xs-12 text-right">
                                        {{--                                        {{$result->links()}}--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- deleteModal -->
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="deleteModalLabel">{{ trans('labels.DeleteOrder') }}</h4>
                        </div>
                        {!! Form::open(array('url' =>'admin/orders/deleteOrder', 'name'=>'deleteOrder', 'id'=>'deleteOrder', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                        {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
                        {!! Form::hidden('orders_id',  '', array('class'=>'form-control', 'id'=>'orders_id')) !!}
                        <div class="modal-body">
                            <p>{{ trans('labels.DeleteOrderText') }}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">{{ trans('labels.Close') }}</button>
                            <button type="submit" class="btn btn-primary"
                                    id="deleteOrder">{{ trans('labels.Delete') }}</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <!-- Main row -->

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script>

        $('#filter').change(function () {
            var val = $(this).children("option:selected").val();
            var selected = 'visible-' + val;

            var slno = 1;
            $('.all-visible').each(function () {
                $(this).removeClass('hidden');
                if ($(this).hasClass(selected) || val == 0)
                    $(this).find('.slno').html(slno++);
                else
                    $(this).addClass('hidden');
            })
        });
    </script>
@endsection
