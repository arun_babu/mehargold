<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>BADA</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" href="{{asset('landing/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/style.css')}}">

</head>

<body>

<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="collapse navbar-collapse d-md-block d-none" id="navbarNav">
                        <ul class="navbar-nav frst-ul">
                            <li class="nav-item">
                                <a class="nav-link" href="/">HOME</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="signup">BECOME A PRTNER</a>
                            </li>
                        </ul>
                    </div>
                    <a class="navbar-brand" href="/">
                        <img src="{{asset('landing/images/logo.svg')}}" alt="">
                        <span></span>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav scnd-ul">
                            <li class="nav-item d-block d-md-none">
                                <a class="nav-link" href="partner.html">BECOME A PRTNER</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">CONTACT US</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="register-form">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Change Password</h1>
            </div>
            <div class="col-md-6">
                <!-- progressbar -->

            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="form-section">
                    <!-- multistep form -->
                    @if(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="">@lang('website.Error'):</span>
                            {!! session('error') !!}

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="">@lang('website.success'):</span>
                            {!! session('success') !!}

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="list-error"></div>
                    <form id="msform" enctype="multipart/form-data" action="{{ URL::to('/processPassword')}}"
                          method="post">
                        {{csrf_field()}}

                        <fieldset>

                            <input type="hidden" name="phone" class="thrd-field" placeholder="Mobile"
                                   value="{{$phone}}">
                            <span>New Password:</span>
                            <input type="password" name="new_password" class="thrd-field second-view new_password"
                                   placeholder="New Password">
                            <span>Confirm Password:</span>
                            <input type="password" name="confirm_password" class="thrd-field confirm_password"
                                   placeholder="Confirm Password">
                            {{--                            <button type="button" name="next" class="action-button" value=""> Sent OTP--}}
                            {{--                            </button>--}}
                            <button type="button" name="next" class="action-button second-view change-password"
                                    value=""> Change
                            </button>

                        </fieldset>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <a href="index.html">
                    <img src="{{asset('landing/images/footer-logo.svg')}}" class="img-fluid" alt="">
                </a>
                <p>Join India’s #1 Reselling platform trusted by 1 Crore+ Resellers who are earning more than
                    ₹25,000 every month!</p>
            </div>
            <div class="col-md-3">
                <h4>Company</h4>
                <ul>
                    <li><a href="">Careers</a></li>
                    <li><a href="">Bada Business Blog</a></li>
                    <li><a href="">Mehar Tech Blog</a></li>
                    <li><a href="">T&C</a></li>
                    <li><a href="">Privacy</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <h4>Contact Us</h4>
                <ul>
                    <li><a href="">+91 1234 567 9999</a></li>
                    <li><a href="">help@badabusiness.com</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="border-bottem"></div>
            </div>
            <div class="col-md-6">
                <div class="social-media">
                    <a href=""><img src="{{asset('landing/images/fb.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/insta.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/tw.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/in.png')}}" class="img-fluid" alt=""></a>
                    <a href=""><img src="{{asset('landing/images/YT.png')}}" class="img-fluid" alt=""></a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="copy-right">
                    <p>© 2015-20 Bada Business Inc. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="{{asset('landing/js/jquery-3.4.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('landing/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('landing/js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('landing')}}" type="text/javascript"></script>
<script src="{{asset('landing/js/slick.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    AOS.init();
</script>
<script type="text/javascript">
    function uploadFile(target) {
        document.getElementById("file-name").innerHTML = target.files[0].name;
    }
</script>

<script>
    $(document).ready(function () {

        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;

        $(".next").click(function () {

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            prev_fs = $(this).parent().prev();

            active_step_val = current_fs.length;
            //  console.log(index(next_fs));

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            // adding new class to already done radios
            $("#progressbar li").eq($("fieldset").index(current_fs)).addClass("step_done");


            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({'opacity': opacity});
                },
                duration: 600
            });
        });

        $(".previous").click(function () {

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //Remove class active
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();

            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    previous_fs.css({'opacity': opacity});
                },
                duration: 600
            });
        });

        $('.radio-group .radio').click(function () {
            $(this).parent().find('.radio').removeClass('selected');
            $(this).addClass('selected');
        });

        $(".submit").click(function () {
            return false;
        })

        $('.change-password').click(function () {
            $('.list-error').html('');
            // Check password is null
            if ($('.new_password').val() && $('.confirm_password').val()) {
                // Check password are same
                if ($('.new_password').val() == $('.confirm_password').val()) {
                    $('#msform').submit();
                } else {
                    var error = '<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
                        '                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n' +
                        '                                <span class="sr-only">Error:</span>\n' +
                        '                                Password mismatch\n' +
                        '                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                        '                                    <span aria-hidden="true">&times;</span>\n' +
                        '                                </button>\n' +
                        '                            </div>';
                    $('.list-error').html(error);


                }
            }


        })

    });
</script>

</body>

</html><?php
