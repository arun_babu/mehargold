@extends('web.layout')
@section('content')

    <div class="container-fuild">
        <nav aria-label="breadcrumb">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ URL::to('/')}}">@lang('website.Home')</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@lang('website.My Orders')</li>
                </ol>
            </div>
        </nav>
    </div>

    <!--My Order Content -->
    <section class="order-one-content pro-content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3  d-none d-lg-block d-xl-block">
                    <div class="heading">
                        <h2>
                            @lang('website.My Account')
                        </h2>
                        <hr>
                    </div>
                    @if(Auth::guard('customer')->check())
                        <ul class="list-group">
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/profile')}}">
                                    <i class="fas fa-user"></i>
                                    @lang('website.Profile')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/wishlist')}}">
                                    <i class="fas fa-heart"></i>
                                    @lang('website.Wishlist')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/orders')}}">
                                    <i class="fas fa-shopping-cart"></i>
                                    @lang('website.Orders')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/shipping-address')}}">
                                    <i class="fas fa-map-marker-alt"></i>
                                    @lang('website.Shipping Address')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/logout')}}">
                                    <i class="fas fa-power-off"></i>
                                    @lang('website.Logout')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/bank-details')}}">
                                    <i class="fas fa-university" aria-hidden="true"></i>
                                    @lang('website.Bank Details')
                                </a>
                            </li>
                        </ul>
                    @elseif(!empty(session('guest_checkout')) and session('guest_checkout') == 1)
                        <ul class="list-group">
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/orders')}}">
                                    <i class="fas fa-shopping-cart"></i>
                                    @lang('website.Orders')
                                </a>
                            </li>
                        </ul>
                    @endif
                </div>
                <div class="col-12 col-lg-9 ">
                    <div class="heading">
                        <h2>
                            @lang('website.Bank Details')
                        </h2>
                        <hr>
                    </div>
                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            {{ session()->get('message') }}
                        </div>

                    @endif



                        <form name="updateMyProfile" class="align-items-center" enctype="multipart/form-data"
                              action="{{ URL::to('store-bank-details')}}" method="post">
                            @csrf
                            @if( count($errors) > 0)
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">@lang('website.Error'):</span>
                                        {{ $error }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endforeach
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session()->get('error') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if(Session::has('error'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">@lang('website.Error'):</span>
                                    {{ session()->get('error') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if(Session::has('error'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">@lang('website.Error'):</span>
                                    {!! session('loginError') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if(session()->has('success') )
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="firstName"
                                       class="col-sm-2 col-form-label">@lang('website.Bank Name')</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="bank_name" class="form-control"
                                           placeholder="@lang('website.Bank Name')">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lastName" class="col-sm-2 col-form-label">@lang('website.account_number')</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="account_number"
                                           placeholder="@lang('website.account_number')" class="form-control field-validate">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lastName" class="col-sm-2 col-form-label">@lang('website.IFSC Code')</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="ifsc_code"
                                           placeholder="@lang('website.IFSC Code')" class="form-control field-validate">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lastName" class="col-sm-2 col-form-label">@lang('website.Phone Number')</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="phone_number"
                                           placeholder="@lang('website.Phone Number')" class="form-control field-validate">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lastName" class="col-sm-2 col-form-label">@lang('website.Status')</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="active">                                        <option value="0">Inactive</option>
                                        <option value="1">Active</option>
                                    </select>
                                </div>
                            </div>

                            <button type="submit"
                                    class="btn btn-secondary swipe-to-top">@lang('website.Save')</button>
                        </form>

                    <!-- ............the end..... -->
                </div>
            </div>
        </div>
    </section>

@endsection
