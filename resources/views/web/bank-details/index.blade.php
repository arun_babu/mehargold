@extends('web.layout')
@section('content')

    <div class="container-fuild">
        <nav aria-label="breadcrumb">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ URL::to('/')}}">@lang('website.Home')</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@lang('website.My Orders')</li>
                </ol>
            </div>
        </nav>
    </div>

    <!--My Order Content -->
    <section class="order-one-content pro-content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3  d-none d-lg-block d-xl-block">
                    <div class="heading">
                        <h2>
                           @lang('website.My Account')
                        </h2>
                        <hr>
                    </div>
                    @if(Auth::guard('customer')->check())
                        <ul class="list-group">
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/profile')}}">
                                    <i class="fas fa-user"></i>
                                    @lang('website.Profile')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/wishlist')}}">
                                    <i class="fas fa-heart"></i>
                                    @lang('website.Wishlist')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/orders')}}">
                                    <i class="fas fa-shopping-cart"></i>
                                    @lang('website.Orders')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/shipping-address')}}">
                                    <i class="fas fa-map-marker-alt"></i>
                                    @lang('website.Shipping Address')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/logout')}}">
                                    <i class="fas fa-power-off"></i>
                                    @lang('website.Logout')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/bank-details')}}">
                                    <i class="fas fa-university" aria-hidden="true"></i>
                                    @lang('website.Bank Details')
                                </a>
                            </li>
                        </ul>
                    @elseif(!empty(session('guest_checkout')) and session('guest_checkout') == 1)
                        <ul class="list-group">
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/orders')}}">
                                    <i class="fas fa-shopping-cart"></i>
                                    @lang('website.Orders')
                                </a>
                            </li>
                        </ul>
                    @endif
                </div>
                <div class="col-12 col-lg-9 ">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="heading">
                                <h2>
                                    @lang('website.Bank Details')
                                </h2>

                            </div>
                        </div>
                        <div class="col-md-6">
                                <a href="create-bank-details" class="pull-right">
                                    <button class="btn btn-primary d-flex justify-content-end"
                                            style=" margin-left: auto;right: 0;margin-right: 20px; position: absolute; top:-20px" >create
                                    </button>
                                </a>

                        </div>
                    </div>
                    <hr>
                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            {{ session()->get('message') }}
                        </div>

                    @endif

                    <table class="table order-table">
                        <thead>
                        <tr class="d-flex">
{{--                            <th class="col-12 col-md-2">@lang('website.ID')</th>--}}
                            <th class="col-12 col-md-2">@lang('website.Bank Name')</th>
                            <th class="col-12 col-md-2">@lang('website.Account Number')</th>
                            <th class="col-12 col-md-2">@lang('website.IFSC Code')</th>
                            <th class="col-12 col-md-2">@lang('website.Phone Number')</th>
                            <th class="col-12 col-md-2">@lang('website.Status')</th>
                            <th class="col-12 col-md-2"> @lang('website.Action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(true)
                            @foreach( $bankDetails as $each)
                                <tr class="d-flex">
{{--                                    <td class="col-12 col-md-2">{{$each->id}}</td>--}}
                                    <td class="col-12 col-md-2">{{$each->bank_name}}</td>
                                    <td class="col-12 col-md-2">
                                        {{$each->account_number}}
                                    </td>
                                    <td class="col-12 col-md-2">
                                        {{$each->ifsc_code}}
                                    </td>
                                    <td class="col-12 col-md-2">
                                        {{$each->phone_number}}
                                    </td>
                                    <td class="col-12 col-md-2">
                                        {{$each->active==1?'Active':'Inactive'}}
                                    </td>
                                    <td align="right" class="col-12 col-md-2"><a
                                                href="{{ URL::to('/edit-bank-details/'.$each->id)}}"><i
                                                    class="fa fa-edit"></i></a>
                                        @if($each->active==0)
                                            <a style="padding-left: 8px"
                                               href="{{ URL::to('/delete-bank-details/'.$each->id)}}"><i
                                                        class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">@lang('website.No order is placed yet')
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <!-- ............the end..... -->
                </div>
            </div>
        </div>
    </section>

@endsection
