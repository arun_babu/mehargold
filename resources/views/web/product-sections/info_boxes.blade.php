<style>
    .adjest-padding{
        padding-left: 20px;
    }
</style>

<section class="boxes-content">
  
    <div class="container">
      <div class="info-boxes-content">         
        <div class="row adjest-padding">

            <div class="col-12 col-md-6 col-lg-3 pl-xl-0">
                <div class="info-box first">
                    <div class="panel">
                        <h3 class="fas fa-truck"></h3>
                        <div class="block">
                            <h4 class="title">@lang('website.bannerLabel1')</h4>
                            <span>@lang('website.bannerLabel1Text')</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 pl-xl-0">
                <div class="info-box">
                    <div class="panel">
                        <h3 class="fas fa-money-bill-alt"></h3>
                        <div class="block">
                            <h4 class="title">@lang('website.bannerLabel2')</h4>
                            <span>@lang('website.bannerLabel2Text')</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 pl-xl-0">
              <div class="info-box">
                  <div class="panel">
                      <h3 class="fas fa-life-ring" style="margin-right: 7px !important;"></h3>
                      <div class="block">
                          <h4 class="title">@lang('website.bannerLabel3')</h4>
                          <span>@lang('website.hotline')&nbsp;:&nbsp;({{$result['commonContent']['setting'][11]->value}})</span>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 pl-xl-0">
                <div class="info-box last">
                    <div class="panel">
                        <h3 class="fas fa-credit-card"></h3>
                        <div class="block">
                            <h4 class="title">@lang('website.bannerLabel4')</h4>
                            <span>@lang('website.bannerLabel4Text')</span>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>
