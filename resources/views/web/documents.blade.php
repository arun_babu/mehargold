@extends('web.layout')
@section('content')

    <div class="container-fuild">
        <nav aria-label="breadcrumb">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ URL::to('/')}}">@lang('website.Home')</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@lang('website.myProfile')</li>

                </ol>
            </div>
        </nav>
    </div>
    <section class="pro-content">
        <!-- Profile Content -->
        <section class="profile-content">
            <div class="container">
                <div class="row">

                    <div class="col-12 media-main">
                        <div class="media">
                            <div class="avatar">
                                <?php
                                if(auth()->guard('customer')->check()){
                                if(auth()->guard('customer')->user()->avatar == null){ ?>
                                <img class="img-fluid"
                                     src="{{asset('web/images/miscellaneous/avatar.jpg')}}">
                                <?php }else{ ?>
                                <img class="img-fluid"
                                     src="{{Storage::disk('s3')->url(auth()->guard('customer')->user()->avatar)}}">
                                <?php
                                }
                                }
                                ?>
                            </div>
                            {{--                            <img src="web/images/miscellaneous/avatar.jpg" alt="avatar">--}}
                            <div class="media-body">
                                <div class="row">
                                    <div class="col-12 col-sm-4 col-md-6">
                                        <h4>{{auth()->guard('customer')->user()->first_name}} {{auth()->guard('customer')->user()->last_name}}
                                            <br>
                                            <small>@lang('website.Phone')
                                                : {{ auth()->guard('customer')->user()->phone }} </small></h4>
                                    </div>
                                    <div class="col-12 col-sm-8 col-md-6 detail">
                                        <p class="mb-0">@lang('website.E-mail')
                                            :<span>{{auth()->guard('customer')->user()->email}}</span></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-lg-3">
                        <div class="heading">
                            <h2>
                                @lang('website.My Account')
                            </h2>
                            <hr>
                        </div>

                        <ul class="list-group">
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/profile')}}">
                                    <i class="fas fa-user"></i>
                                    @lang('website.Profile')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/wishlist')}}">
                                    <i class="fas fa-heart"></i>
                                    @lang('website.Wishlist')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/orders')}}">
                                    <i class="fas fa-shopping-cart"></i>
                                    @lang('website.Orders')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/shipping-address')}}">
                                    <i class="fas fa-map-marker-alt"></i>
                                    @lang('website.Shipping Address')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/logout')}}">
                                    <i class="fas fa-power-off"></i>
                                    @lang('website.Logout')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/change-password')}}">
                                    <i class="fas fa-unlock-alt"></i>
                                    @lang('website.Change Password')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/earnings')}}">
                                    <i class="fas fa-credit-card"></i>
                                    @lang('website.Earnings')
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="nav-link" href="{{ URL::to('/bank-details')}}">
                                    <i class="fas fa-university" aria-hidden="true"></i>
                                    @lang('website.Bank Details')
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-9 ">
                        <div class="heading">
                            <div class="container-fluid">
                                <ul id="myTabs" class="nav nav-tabs nav-fill" style="width: 100%">
                                    <li class="nav-item active" style="width: 33% ">
                                        <a class="nav-link active" href="/profile"><h5>Basic Details</h5></a>
                                    </li>
                                    <li class="nav-item" style="width: 33%">
                                        <a class="nav-link active" href="/profile-details"><h5>Personal Details</h5></a>
                                    </li>
                                    <li class="nav-item" style="width: 33%">
                                        <a class="nav-link active" href="/documents"
                                           style="color: #337ab7;background-color: #ffffff"><h5>Documents</h5></a>
                                    </li>

                                </ul>

                            </div>
                            {{--                            <hr>--}}
                        </div>

                        <form name="updateMyProfile" class="align-items-center" enctype="multipart/form-data"
                              action="{{ URL::to('updateDocuments')}}" method="post">
                            <input value="{{auth()->guard('customer')->user()->id}}" name="userId" type="hidden">
                            <input value="{{$profileDetails?$profileDetails->id:''}}" name="id" type="hidden">
                            @csrf
                            @if( count($errors) > 0)
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">@lang('website.Error'):</span>
                                        {{ $error }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endforeach
                            @endif

                            @if(session()->has('error'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session()->get('error') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if(Session::has('error'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">@lang('website.Error'):</span>
                                    {{ session()->get('error') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if(Session::has('error'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">@lang('website.Error'):</span>
                                    {!! session('loginError') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if(session()->has('success') )
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="form-group row">

                                <div class=" col-sm-6 ">
                                    <div class="avatar d-flex justify-content-center" style="height: 150px">
                                        <?php
                                        if(auth()->guard('customer')->check()){
                                        if(auth()->guard('customer')->user()->avatar == null){ ?>
                                        <img class="img-fluid" width="150px"
                                             src="{{asset('web/images/miscellaneous/avatar.jpg')}}">
                                        <?php }else{ ?>
                                        <img class="img-fluid"
                                             src="{{Storage::disk('s3')->url(auth()->guard('customer')->user()->avatar)}}">
                                        <?php
                                        }
                                        }
                                        ?>
                                    </div>
                                    <div class=" d-flex mt-4 ">
                                        <label for="district"
                                               class="col-sm-4 col-form-label">@lang('website.Profile pic')</label>
                                        <div class="from-group  select-control col-sm-8 ">
                                            <input type="file" name="photo" id="file"
                                                   class="form-control field-validate"
                                                   style="padding-top: 3px">
                                        </div>
                                    </div>

                                </div>


                                <div class="from-group col-sm-6 ">
                                    <div class="avatar d-flex justify-content-center" style="height: 150px">
                                        <?php
                                        if(auth()->guard('customer')->check()){
                                        if(true){ ?>
                                        <img class="img-fluid" width="150px"
                                             src="{{asset('web/images/miscellaneous/document.png')}}">
                                        <?php }else{ ?>
                                        <img class="img-fluid" width="150px"
                                             src="{{Storage::disk('s3')->url(auth()->guard('customer')->user()->avatar)}}">
                                        <?php
                                        }
                                        }
                                        ?>
                                    </div>
                                    <div class=" d-flex mt-4 ">

                                        <label for="district"
                                               class="col-sm-4 col-form-label">@lang('website.Doc Upload')</label>
                                        <div class="from-group  select-control col-sm-8 ">
                                            <input type="file" name="doc" id="file" class="form-control field-validate"
                                                   style="padding-top: 3px">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <button type="submit"
                                    class="btn btn-secondary swipe-to-top">@lang('website.Update')</button>
                        </form>

                        <!-- ............the end..... -->
                    </div>
                </div>
            </div>
        </section>
        </div>
    </section>
    <script>
        $('#country').change(function () {
            var country = $(this).val();
            $.ajax({
                url: '/getStates/' + country,
            }).done(function (e) {
                var option = '<option value="0">Select Option</option>';
                $.each(e, function (key, val) {
                    option += '<option value="' + val.id + '">' + val.state + '</option>'
                });
                $('#state').html(option);
            })
        });
        $('#state').change(function () {
            var state = $(this).val();
            $.ajax({
                url: '/getDistricts/' + state,
            }).done(function (e) {
                var option = '<option value="0">Select Option</option>';
                $.each(e, function (key, val) {
                    option += '<option value="' + val.id + '">' + val.district + '</option>'
                });
                $('#district').html(option)
            })
        })
        $('#district').change(function () {
            var district = $(this).val();
            $.ajax({
                url: '/getTaluk/' + district,
            }).done(function (e) {
                var option = '<option value="0">Select Option</option>';
                $.each(e, function (key, val) {
                    option += '<option value="' + val.id + '">' + val.taluk + '</option>'
                });
                $('#taluk').html(option)
            })
        })
    </script>
@endsection

