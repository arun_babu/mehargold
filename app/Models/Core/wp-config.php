<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_cy_altmed' );

/** MySQL database username */
define( 'DB_USER', 'altmed' );

/** MySQL database password */
define( 'DB_PASSWORD', 'altmed' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NQsIsO3IxoyQDtatwMRVP2Rt1xBaUXGwFgWnGSjxRE2moZjIBsxR4VBOwzOSNrnQ');
define('SECURE_AUTH_KEY',  'IFD76hC2x9n0PrYEkO6n2gHqxPaVlvZ7Qr0RscMKEEzt7KfygnzgyAS75TarRO4D');
define('LOGGED_IN_KEY',    'hwoDfac57rAPacT8BLWBZ13cFmKu1b8D8KTT2C03KpkiEHQyH3na2b0I02PbKd0h');
define('NONCE_KEY',        'J1WllWmjRu46HVtIdrt47v9tefLFHm9PeO4fzuNxBBoLEbXNXnfYSWB1G0F0abze');
define('AUTH_SALT',        'G45niGcjI7aPGtRRQmZSJwKtgScb6GEm8EC03ADN79zOue5Rs50XBlwfPBGsUYJi');
define('SECURE_AUTH_SALT', 'jA2Zmq7Pd4IIsWwC1vknIBIC2JW9sffx96LbGL6mhJYLiWkwVvgR0zYDindIU5C1');
define('LOGGED_IN_SALT',   'Mh4GrUIUwfpZSKF5sKKnJgFhEzgpfXIx0gFC9RUJptQmOmeX3gsgOAH4K1wvnaHu');
define('NONCE_SALT',       'NiSFNnfGCaWKby16ea5yGFJU1NRl5GAkG2XtzsqC36YeDMghSw6OOM2eHkYBWpkT');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');
define('FS_CHMOD_DIR',0755);
define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed externally by Installatron.
 * If you remove this define() to re-enable WordPress's automatic background updating
 * then it's advised to disable auto-updating in Installatron.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
