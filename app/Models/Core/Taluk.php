<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class Taluk extends Model
{
    //
    use Sortable;
    protected $table = 'taluk';

    public function paginator()
    {
        $zones = Taluk::sortable(['id' => 'ASC'])
            ->LeftJoin('districts','districts.id','=','taluk.district_id')
            ->select('taluk.id as id','taluk.district_id','taluk.taluk','districts.district as district')
            ->paginate(30);

        return $zones;
    }

    public function insert($request)
    {
        $taluk_id = DB::table('taluk')->insertGetId([
            'district_id' => $request->district_id,
            'taluk' => $request->taluk
        ]);
        return $taluk_id;
    }
    public function edit($request){
        $zones =  DB::table('taluk')
            ->where('id', $request->id)
            ->first();

        return $zones;
    }
    public function updaterecord($request){
        DB::table('taluk')->where('id', $request->id)->update([
            'district_id' => $request->district_id,
            'taluk' => $request->taluk
        ]);
    }

    public function deleterecord($request){
        DB::table('taluk')->where('id', $request->id)->delete();
    }

    public function filter($data){
        $name = $data['FilterBy'];
        $param = $data['parameter'];

        $result = array();
        $message = array();
        $errorMessage = array();

        switch ( $name ) {
            case 'Taluk':
                $taluks = Taluk::sortable(['id'=>'ASC'])->where('taluk', 'LIKE', '%' . $param . '%')
                    ->LeftJoin('districts','districts.id','=','taluk.district_id')
                    ->select('taluk.id as id','taluk.district_id','taluk.taluk','districts.district as district')
                    ->paginate(30);
                break;
            case 'District':
                $taluks = Taluk::sortable(['id'=>'ASC'])->where('district', 'LIKE', '%' . $param . '%')
                    ->LeftJoin('districts','districts.id','=','taluk.district_id')
                    ->select('taluk.id as id','taluk.district_id','taluk.taluk','districts.district as district')
                    ->paginate(30);
                break;
            default:
                $taluks = Zones::sortable(['id'=>'ASC'])
                    ->LeftJoin('districts','districts.id','=','taluk.district_id')
                    ->select('taluk.id as id','taluk.district_id','taluk.taluk','districts.district as district')

                    ->paginate(30);
                break;
        }

        return $taluks;
    }

}
