<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class District extends Model
{
    //
    use Sortable;
    public $sortable = ['id'];

    public function paginator()
    {
        $district = District::sortable(['id' => 'ASC'])
            ->LeftJoin('zones', 'zones.zone_id', '=', 'districts.state_id')
            ->paginate(30);
        return $district;
    }

    public function insert($request)
    {
        $zone_id = DB::table('districts')->insertGetId([
            'state_id' => $request->state_id,
//            'zone_code'	 		=>   $request->zone_code,
            'district' => $request->district
        ]);
        return $zone_id;
    }

    public function edit($request)
    {
        $districts = DB::table('districts')->where('id', $request->id)->first();
        return $districts;
    }

    public function updaterecord($request)
    {
        DB::table('districts')->where('id', $request->zone_id)->update([
            'state_id' => $request->state_id,
            'district' => $request->district
        ]);
    }

    public function deleterecord($request)
    {
        DB::table('districts')->where('id', $request->id)->delete();
    }

    public function filter($data)
    {
        $name = $data['FilterBy'];
        $param = $data['parameter'];

        $result = array();
        $message = array();
        $errorMessage = array();

        switch ($name) {
            case 'District':
                $zones = District::sortable(['id' => 'ASC'])->where('district', 'LIKE', '%' . $param . '%')
                    ->LeftJoin('zones', 'zones.zone_id', '=', 'districts.state_id')
                    ->paginate(30);
                break;
            case 'State':
                $zones = District::sortable(['id' => 'ASC'])->where('zone_name', 'LIKE', '%' . $param . '%')
                    ->LeftJoin('zones', 'zones.zone_id', '=', 'districts.state_id')
                    ->paginate(30);
                break;

            default:
                $zones = District::sortable(['id' => 'ASC'])
                    ->LeftJoin('zones', 'zones.zone_id', '=', 'districts.state_id')
                    ->paginate(30);
                break;
        }

        return $zones;
    }

}
