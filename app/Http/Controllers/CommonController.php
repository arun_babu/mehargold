<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class CommonController extends Controller
{
    //
    public function s3Manager($imageobject, $code_string)
    {


        $imageHeight = getimagesize($imageobject)[1];

        $imgPath = $imageobject;
        $image = '';
        if ($imageobject->extension() == 'jpeg'||$imageobject->extension() == 'jpg') {
            $image = imagecreatefromjpeg($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefrompng($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefromgif($imgPath);
        }
//        $color = imagecolorallocate($image, 0, 0, 0);
        $string = strtoupper($code_string);
//        $x = 2;
//        $y = $fontHeight - 20;
//        $font = public_path('fonts/calibri.ttf');
//        $bg_width = strlen($string) * 16;
//        $color1 = imagecolorallocate($image, 225, 224, 224);
//        imagefilledrectangle($image, $x, $y, $bg_width, $y - 20, $color1);

//        imagettftext($image, 15, null, $x, $y, $color, $font, $string);
//        imagestring($image, $fontSize, $x, $y, $string, $color);
//        ob_start();
//        imagejpeg($image);
//        $image_contents = ob_get_clean();
        $textsize = $string . ' ';

        $image_contents = $this->mergeImage($image, $textsize, $imageHeight);
//S3-----
        $path = time() . $imageobject->getClientOriginalName();
        $filePath = 'images/' . date("Y") . '/' . date("m") . '/' . $path;
        $t = Storage::disk('s3')->put($filePath, $image_contents);
//        $imageName = Storage::disk('s3')->url($filePath);

        return $filePath;
    }

    public function s3Media($Path, $image)
    {
        $t = Storage::disk('s3')->put($Path, $image);
        $Path_s3 = Storage::disk('s3')->url($Path);
        return $Path;
    }


    public function s3MediaWithCode($Path, $imageobject, $code_string)
    {

        $imageHeight = getimagesize($imageobject)[1];
        $imgPath = $imageobject;
        $image = '';
        if ($imageobject->extension() == 'jpeg'||$imageobject->extension() == 'jpg') {
            $image = imagecreatefromjpeg($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefrompng($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefromgif($imgPath);
        }
        $string = strtoupper($code_string);


//        imagettftext($image, 15, null, $x, $y, $color, $font, $string);
//        ob_start();
//        imagejpeg($image);
//        $image_contents = ob_get_clean();
//S3-----
        $textsize = $string . ' ';
        $image_contents = $this->mergeImage($image, $textsize, $imageHeight);

        $t = Storage::disk('s3')->put($Path, $image_contents);
        $Path_s3 = Storage::disk('s3')->url($Path);
        return $Path;
    }


    public function s3MediaWithCodeExtra($Path, $imageobject, $code_string, $height)
    {

        $imageHeight = $height;
        $info = getimagesize($Path);
        $extension = image_type_to_extension($info[2]);
        $ext = str_replace('.', '', $extension);
        if ($ext == 'jpeg' || $ext == 'jpg') {
            $image = imagecreatefromjpeg($Path);
        } else if ($ext == 'png') {
            $image = imagecreatefrompng($Path);
        } else if ($ext == 'png') {
            $image = imagecreatefromgif($Path);
        }
        $string = strtoupper($code_string);
        $textsize = $string . ' ';
        $image = $this->mergeImage($image, $textsize, $imageHeight);

//S3-----
        $t = Storage::disk('s3')->put($Path, $image);
        $Path_s3 = Storage::disk('s3')->url($Path);
        return $Path;
    }


    public function mergeImage($image, $text, $height_im = 10)
    {
        $spacing = $height_im / 134;
        $inputsize = $height_im / 33;
//        $font = public_path('fonts/calibri.ttf');
        $font = public_path('fonts/bahnschrift.ttf');
        $bbox = imagettfbbox($inputsize, 0, $font, $text);
        $minX = min(array($bbox[0], $bbox[2], $bbox[4], $bbox[6]));
        $maxX = max(array($bbox[0], $bbox[2], $bbox[4], $bbox[6]));
        $minY = min(array($bbox[1], $bbox[3], $bbox[5], $bbox[7]));
        $maxY = max(array($bbox[1], $bbox[3], $bbox[5], $bbox[7]));
        $boxwidth = $maxX - $minX;
        $boxheight = $maxY - $minY;
        $im = imagecreatetruecolor($boxwidth, $boxheight + 2);
        $black = imagecolorallocate($im, 0, 0, 0);
        $white = imagecolorallocate($im, 255, 255, 255);
        imagefilledrectangle($im, 0, 0, $boxwidth-6, $boxheight+4, $white);
        $x = $bbox[0] + (imagesx($im) / 2) - ($bbox[4] / 2);
        $y = $bbox[1] + (imagesy($im) / 2) - ($bbox[5] / 2);
        imagettftext($im, $inputsize, 0, $x+2, $y, $black, $font, $text);

//test
//        $red = imagecolorallocate($im, 243, 12, 12);

//        $radius = 8;
//        $x1 = 0;
//        $x2 = $boxwidth;
//        $y1 = 0;
//        $y2 = $boxheight;
//        $color = $red;
//
//        imagefilledrectangle($im, $x1 + $radius, $y1, $x2 - $radius, $y2, $color);
////        imagefilledrectangle($im, $x1, $y1 + $radius, $x2, $y2 - $radius, $color);
//// draw circled corners
//        imagefilledellipse($im, $x1 + $radius, $y1 + $radius, $radius * 2, $radius * 2, $color);
//        imagefilledellipse($im, $x2 - $radius, $y1 + $radius, $radius * 2, $radius * 2, $color);
//        imagefilledellipse($im, $x1 , $y2 , $radius , $radius , $color);
//        imagefilledellipse($im, $x2 , $y2 , $radius , $radius , $color);
//
//        Header("Content-type: image/png");
//        imagepng($im);
//        var_dump('sda');
//        die();
//test

        // Output to browser
//            imagepng($im);
//        imagedestroy($im);
        $stamp = $im;
        $im = $image;
// Set the margins for the stamp and get the height/width of the stamp image
        $marge_right = $spacing;
//        $marge_bottom = $spacing;
        $marge_bottom = $height_im / 95;
        $sx = imagesx($stamp);
        $sy = imagesy($stamp);
// Copy the stamp image onto our photo using the margin offsets and the photo
// width to calculate positioning of the stamp.
        imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
// Output and free memory

//        imagepng($im);
        ob_start();
        imagepng($im);
        $image_contents = ob_get_clean();

        return $image_contents;
    }

}
