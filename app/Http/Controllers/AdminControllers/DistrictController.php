<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Core\District;
use App\Models\Core\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;

class DistrictController extends Controller
{
    //
    public function __construct(District $district, Setting $setting)
    {
        $this->District = $district;
        $this->Setting = $setting;
    }

    public function index()
    {
        $title = array('pageTitle' => Lang::get("labels.ListingZones"));
        $district = $this->District->paginator();
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.district.index", $title)->with('result', $result)->with('district', $district);
    }

    public function add()
    {
        $title = array('pageTitle' => Lang::get("labels.AddZone"));
        $result = array();
        $message = array();
        $states = DB::table('zones')->get();
        $result['countries'] = $states;
        $result['message'] = $message;
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.district.add", $title)->with('result', $result);
    }

    public function insert(Request $request)
    {
        $this->District->insert($request);
        $message = Lang::get("labels.ZoneAddedMessage");
        return Redirect::back()->with('message', $message);
    }

    public function edit(Request $request)
    {
        $title = array('pageTitle' => Lang::get("labels.EditZone"));
        $result = array();
        $result['message'] = array();

        $zones = $this->District->edit($request);
        $states = DB::table('zones')->get();
        $result['countries'] = $states;
        $result['zones'] = $zones;
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.district.edit", $title)->with('result', $result);
    }

    public function update(Request $request)
    {
        $this->District->updaterecord($request);
        $countryData['message'] = 'Zone has been updated successfully!';
        $message = Lang::get("labels.Zone has been updated successfully");
        return Redirect::back()->with('message', $message);
    }

    public function delete(Request $request)
    {
        $this->District->deleterecord($request);
        return redirect()->back()->withErrors([Lang::get("labels.ZoneDeletedTax")]);
    }
    public function filter(Request $request)
    {
        $name = $request->FilterBy;
        $param = $request->parameter;
        $title = array('pageTitle' => Lang::get("labels.ListingZones"));
        $district = $this->District->filter($request);
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.district.index", $title)->with('result', $result)->with('district', $district)->with('name', $name)->with('param', $param);
    }

}
