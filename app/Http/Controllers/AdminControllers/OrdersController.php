<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\AdminControllers\SiteSettingController;
use App\Http\Controllers\Controller;
use App\Models\Core\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Models\Core\Order;
Use App\User;


class OrdersController extends Controller
{
    //
    public function __construct(Setting $setting, Order $order)
    {
        $this->myVarsetting = new SiteSettingController($setting);
        $this->Setting = $setting;
        $this->Order = $order;
    }

    //add listingOrders
    public function display()
    {
        $title = array('pageTitle' => Lang::get("labels.ListingOrders"));

        $message = array();
        $errorMessage = array();

        $ordersData['orders'] = $this->Order->paginator();
        $ordersData['message'] = $message;
        $ordersData['errorMessage'] = $errorMessage;
        $ordersData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.Orders.index", $title)->with('listingOrders', $ordersData)->with('result', $result);
    }

    //view order detail
    public function vieworder(Request $request)
    {

        $title = array('pageTitle' => Lang::get("labels.ViewOrder"));
        $message = array();
        $errorMessage = array();

        //orders data
        $ordersData = $this->Order->detail($request);
        // current order status
        $orders_status_history = $this->Order->currentOrderStatus($request);
        //all statuses
        $orders_status = $this->Order->orderStatuses();
        $ordersData['message'] = $message;
        $ordersData['errorMessage'] = $errorMessage;
        $ordersData['orders_status'] = $orders_status;
        $ordersData['orders_status_history'] = $orders_status_history;
//        $customerData = DB::table('profile_details')
//            ->where('user_id', $ordersData['orders_data'][0]->customers_id)
//            ->first();
//var_dump($ordersData['orders_data'][0]->customers_id);
//var_dump($customerData);
//die();
        $customerData = DB::table('users')
            ->where('users.id', $ordersData['orders_data'][0]->customers_id)
            ->join('profile_details', 'profile_details.user_id', '=', 'users.id')
            ->first();

        if ($customerData) {
            $countries_name = DB::table('countries')->where('countries_id', $customerData->country)->first();
            $customerData->countries_name = $countries_name ? $countries_name->countries_name : '';
            $zone_name = DB::table('zones')->where('zone_id', $customerData->state)->first();
            $customerData->zone_name = $zone_name ? $zone_name->zone_name : '';
            $district = DB::table('districts')->where('id', $customerData->district)->first();
            $customerData->district = $district ? $district->district : '';
            $taluk = DB::table('taluk')->where('id', $customerData->taluk)->first();
            $customerData->taluk = $taluk ? $taluk->taluk : '';


        } else {
            $customerData = DB::table('users')
                ->where('users.id', $ordersData['orders_data'][0]->customers_id)
                ->first();
            $customerData->countries_name = '';
            $customerData->zone_name = '';
            $customerData->district = '';
            $customerData->taluk = '';
        }
        //get function from other controller
        $ordersData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();

        //dd($ordersData);
        return view("admin.Orders.vieworder", $title)->with('data', $ordersData)->with('result', $result)->with('customerData', $customerData);
    }

    //update order
    public function updateOrder(Request $request)
    {

        $orders_status = $request->orders_status;
        $old_orders_status = $request->old_orders_status;

        $comments = $request->comments;
        $orders_id = $request->orders_id;

        //get function from other controller
        $setting = $this->myVarsetting->getSetting();

        if ($old_orders_status == $orders_status) {
            return redirect()->back()->with('error', Lang::get("labels.StatusChangeError"));
        } else {
            //update order
            $orders_status = $this->Order->updateRecord($request);
            return redirect()->back()->with('message', Lang::get("labels.OrderStatusChangedMessage"));
        }

    }

    //deleteorders
    public function deleteOrder(Request $request)
    {
        //reverse stock
        $this->Order->reverseStock($request);
        $this->Order->deleteRecord($request);

        return redirect()->back()->withErrors([Lang::get("labels.OrderDeletedMessage")]);
    }

    //view order detail
    public function invoiceprint(Request $request)
    {

        $title = array('pageTitle' => Lang::get("labels.ViewOrder"));
        $language_id = '1';
        $orders_id = $request->id;

        $message = array();
        $errorMessage = array();

        DB::table('orders')->where('orders_id', '=', $orders_id)
            ->where('customers_id', '!=', '')->update(['is_seen' => 1]);

        $order = DB::table('orders')
            ->LeftJoin('orders_status_history', 'orders_status_history.orders_id', '=', 'orders.orders_id')
            ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
            ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
            ->where('orders_status_description.language_id', '=', $language_id)
            ->where('role_id', '<=', 2)
            ->where('orders.orders_id', '=', $orders_id)
            ->orderby('orders_status_history.date_added', 'DESC')
            ->get();

        foreach ($order as $data) {
            $orders_id = $data->orders_id;
            $orders_products = DB::table('orders_products')
                ->join('products', 'products.products_id', '=', 'orders_products.products_id')
                ->LeftJoin('image_categories', function ($join) {
                    $join->on('image_categories.image_id', '=', 'products.products_image')
                        ->where(function ($query) {
                            $query->where('image_categories.image_type', '=', 'THUMBNAIL')
                                ->where('image_categories.image_type', '!=', 'THUMBNAIL')
                                ->orWhere('image_categories.image_type', '=', 'ACTUAL');
                        });
                })
                ->select('orders_products.*', 'image_categories.path as image')
                ->where('orders_products.orders_id', '=', $orders_id)
                ->get();


            $i = 0;
            $total_price = 0;
            $total_tax = 0;
            $product = array();
            $subtotal = 0;
            foreach ($orders_products as $orders_products_data) {

                //categories
                $categories = DB::table('products_to_categories')
                    ->leftjoin('categories', 'categories.categories_id', 'products_to_categories.categories_id')
                    ->leftjoin('categories_description', 'categories_description.categories_id', 'products_to_categories.categories_id')
                    ->select('categories.categories_id', 'categories_description.categories_name', 'categories.categories_image', 'categories.categories_icon', 'categories.parent_id')
                    ->where('products_id', '=', $orders_products_data->orders_products_id)
                    ->where('categories_description.language_id', '=', $language_id)->get();

                $orders_products_data->categories = $categories;

                $product_attribute = DB::table('orders_products_attributes')
                    ->where([
                        ['orders_products_id', '=', $orders_products_data->orders_products_id],
                        ['orders_id', '=', $orders_products_data->orders_id],
                    ])
                    ->get();
                $product_row = DB::table('products')->where('products_id', $orders_products_data->orders_products_id)->first();
//                $orders_products_data->product_code = $product_row->product_code;
                $orders_products_data->product_code = '';
                $orders_products_data->attribute = $product_attribute;
                $product[$i] = $orders_products_data;
                $total_price = $total_price + $orders_products[$i]->final_price;

                $subtotal += $orders_products[$i]->final_price;

                $i++;
            }
            $data->data = $product;
            $orders_data[] = $data;
        }

        $orders_status_history = DB::table('orders_status_history')
            ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
            ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
            ->where('orders_status_description.language_id', '=', $language_id)
            ->where('role_id', '<=', 2)
            ->orderBy('orders_status_history.date_added', 'desc')
            ->where('orders_id', '=', $orders_id)->get();

        $orders_status = DB::table('orders_status')
            ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
            ->where('orders_status_description.language_id', '=', $language_id)
            ->where('role_id', '<=', 2)->get();

        $ordersData['message'] = $message;
        $ordersData['errorMessage'] = $errorMessage;
        $ordersData['orders_data'] = $orders_data;
        $ordersData['total_price'] = $total_price;
        $ordersData['orders_status'] = $orders_status;
        $ordersData['orders_status_history'] = $orders_status_history;
        $ordersData['subtotal'] = $subtotal;

        $customerData = DB::table('users')
            ->where('users.id', $ordersData['orders_data'][0]->customers_id)
            ->join('profile_details', 'profile_details.user_id', '=', 'users.id')
            ->first();

        if ($customerData) {
            $countries_name = DB::table('countries')->where('countries_id', $customerData->country)->first();
            $customerData->countries_name = $countries_name ? $countries_name->countries_name : '';
            $zone_name = DB::table('zones')->where('zone_id', $customerData->state)->first();
            $customerData->zone_name = $zone_name ? $zone_name->zone_name : '';
            $district = DB::table('districts')->where('id', $customerData->state)->first();
            $customerData->district = $district ? $district->district : '';
            $taluk = DB::table('taluk')->where('id', $customerData->taluk)->first();
            $customerData->taluk = $taluk ? $taluk->taluk : '';

        } else {
            $customerData = DB::table('users')
                ->where('users.id', $ordersData['orders_data'][0]->customers_id)
                ->first();
            $customerData->countries_name = '';
            $customerData->zone_name = '';
            $customerData->district = '';
            $customerData->taluk = '';
        }


        //get function from other controller

        $ordersData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();

        return view("admin.Orders.invoiceprint", $title)->with('data', $ordersData)->with('result', $result)->with('customerData', $customerData);

    }

    //view order label print
    public function labelprint(Request $request)
    {
        $title = array('pageTitle' => Lang::get("labels.ViewOrder"));
        $language_id = '1';
        $orders_id = $request->id;
        $message = array();
        $errorMessage = array();

        DB::table('orders')->where('orders_id', '=', $orders_id)
            ->where('customers_id', '!=', '')->update(['is_seen' => 1]);

        $order = DB::table('orders')
            ->LeftJoin('orders_status_history', 'orders_status_history.orders_id', '=', 'orders.orders_id')
            ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
            ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
            ->where('orders_status_description.language_id', '=', $language_id)->where('role_id', '<=', 2)
            ->where('orders.orders_id', '=', $orders_id)->orderby('orders_status_history.date_added', 'DESC')->get();

        foreach ($order as $data) {
            $orders_id = $data->orders_id;
//get image
            $orders_products = DB::table('orders_products')
                ->join('products', 'products.products_id', '=', 'orders_products.products_id')
                ->LeftJoin('image_categories', function ($join) {
                    $join->on('image_categories.image_id', '=', 'products.products_image')
                        ->where(function ($query) {
                            $query->where('image_categories.image_type', '=', 'THUMBNAIL')
                                ->where('image_categories.image_type', '!=', 'THUMBNAIL')
                                ->orWhere('image_categories.image_type', '=', 'ACTUAL');
                        });
                })
                ->select('orders_products.*', 'image_categories.path as image')
                ->where('orders_products.orders_id', '=', $orders_id)->get();


            $i = 0;
            $total_price = 0;
            $total_tax = 0;
            $product = array();
            $subtotal = 0;
            foreach ($orders_products as $orders_products_data) {

                //categories
                $categories = DB::table('products_to_categories')
                    ->leftjoin('categories', 'categories.categories_id', 'products_to_categories.categories_id')
                    ->leftjoin('categories_description', 'categories_description.categories_id', 'products_to_categories.categories_id')
                    ->select('categories.categories_id', 'categories_description.categories_name', 'categories.categories_image', 'categories.categories_icon', 'categories.parent_id')
                    ->where('products_id', '=', $orders_products_data->orders_products_id)
                    ->where('categories_description.language_id', '=', $language_id)->get();

                $orders_products_data->categories = $categories;

                $product_attribute = DB::table('orders_products_attributes')
                    ->where([
                        ['orders_products_id', '=', $orders_products_data->orders_products_id],
                        ['orders_id', '=', $orders_products_data->orders_id],
                    ])
                    ->get();


                $product_row = DB::table('products')
                    ->where('products_id', $orders_products_data->products_id)
                    ->first();

                $orders_products_data->image = $orders_products_data->image;
                if ($orders_products_data->default_image) {
                    $orders_products_data->image = $orders_products_data->default_image;
                }
                $orders_products_data->product_code = $product_row->product_code;
                $orders_products_data->attribute = $product_attribute;
                $product[$i] = $orders_products_data;
                $total_price = $total_price + $orders_products[$i]->final_price;
                $subtotal += $orders_products[$i]->final_price;
                $i++;
            }
            $data->data = $product;
            $orders_data[] = $data;
        }

        $orders_status_history = DB::table('orders_status_history')
            ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
            ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
            ->where('orders_status_description.language_id', '=', $language_id)
            ->where('role_id', '<=', 2)
            ->orderBy('orders_status_history.date_added', 'desc')
            ->where('orders_id', '=', $orders_id)->get();

        $orders_status = DB::table('orders_status')
            ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
            ->where('orders_status_description.language_id', '=', $language_id)
            ->where('role_id', '<=', 2)->get();

        $ordersData['message'] = $message;
        $ordersData['errorMessage'] = $errorMessage;
        $ordersData['orders_data'] = $orders_data;
        $ordersData['total_price'] = $total_price;
        $ordersData['orders_status'] = $orders_status;
        $ordersData['orders_status_history'] = $orders_status_history;
        $ordersData['subtotal'] = $subtotal;
        //get function from other controller
        $ordersData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.Orders.labelPrint", $title)->with('data', $ordersData)->with('result', $result);


    }

    public
    function orderlabelprint(Request $request)
    {

        $title = array('pageTitle' => Lang::get("labels.ViewOrder"));
        $language_id = '1';
        $orders_id = $request->id;

        $message = array();
        $errorMessage = array();

        DB::table('orders')->where('orders_id', '=', $orders_id)
            ->where('customers_id', '!=', '')->update(['is_seen' => 1]);

        $order = DB::table('orders')
            ->LeftJoin('orders_status_history', 'orders_status_history.orders_id', '=', 'orders.orders_id')
            ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
            ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
            ->where('orders_status_description.language_id', '=', $language_id)->where('role_id', '<=', 2)
            ->where('orders.orders_id', '=', $orders_id)->orderby('orders_status_history.date_added', 'DESC')->get();
        foreach ($order as $data) {
            $orders_id = $data->orders_id;

            $orders_products = DB::table('orders_products')
                ->join('products', 'products.products_id', '=', 'orders_products.products_id')
                ->LeftJoin('image_categories', function ($join) {
                    $join->on('image_categories.image_id', '=', 'products.products_image')
                        ->where(function ($query) {
                            $query->where('image_categories.image_type', '=', 'THUMBNAIL')
                                ->where('image_categories.image_type', '!=', 'THUMBNAIL')
                                ->orWhere('image_categories.image_type', '=', 'ACTUAL');
                        });
                })
                ->select('orders_products.*', 'image_categories.path as image')
                ->where('orders_products.orders_id', '=', $orders_id)->get();


            $i = 0;
            $total_price = 0;
            $total_tax = 0;
            $product = array();
            $subtotal = 0;
            foreach ($orders_products as $orders_products_data) {

                //categories
                $categories = DB::table('products_to_categories')
                    ->leftjoin('categories', 'categories.categories_id', 'products_to_categories.categories_id')
                    ->leftjoin('categories_description', 'categories_description.categories_id', 'products_to_categories.categories_id')
                    ->select('categories.categories_id', 'categories_description.categories_name', 'categories.categories_image', 'categories.categories_icon', 'categories.parent_id')
                    ->where('products_id', '=', $orders_products_data->orders_products_id)
                    ->where('categories_description.language_id', '=', $language_id)->get();

                $orders_products_data->categories = $categories;

                $product_attribute = DB::table('orders_products_attributes')
                    ->where([
                        ['orders_products_id', '=', $orders_products_data->orders_products_id],
                        ['orders_id', '=', $orders_products_data->orders_id],
                    ])
                    ->get();
                $product_row = DB::table('products')->where('products_id', $orders_products_data->orders_products_id)->first();

                $orders_products_data->image = $orders_products_data->image;
//                $orders_products_data->product_code = $product_row->product_code;
                $orders_products_data->attribute = $product_attribute;
                $product[$i] = $orders_products_data;
                $total_price = $total_price + $orders_products[$i]->final_price;

                $subtotal += $orders_products[$i]->final_price;

                $i++;
            }
            $data->data = $product;
            $orders_data[] = $data;
        }

        $orders_status_history = DB::table('orders_status_history')
            ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
            ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
            ->where('orders_status_description.language_id', '=', $language_id)
            ->where('role_id', '<=', 2)
            ->orderBy('orders_status_history.date_added', 'desc')
            ->where('orders_id', '=', $orders_id)->get();

        $orders_status = DB::table('orders_status')
            ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
            ->where('orders_status_description.language_id', '=', $language_id)
            ->where('role_id', '<=', 2)->get();

        $ordersData['message'] = $message;
        $ordersData['errorMessage'] = $errorMessage;
        $ordersData['orders_data'] = $orders_data;
        $ordersData['total_price'] = $total_price;
        $ordersData['orders_status'] = $orders_status;
        $ordersData['orders_status_history'] = $orders_status_history;
        $ordersData['subtotal'] = $subtotal;

        //get function from other controller

        $ordersData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.Orders.orderlabelprint", $title)->with('data', $ordersData)->with('result', $result);

    }

    public
    function payouts()
    {
        $title = array('pageTitle' => Lang::get("labels.ListingOrders"));

        $message = array();
        $errorMessage = array();

        $payoutsData['orders'] = $this->Order->paginator();
        $payoutsData['message'] = $message;
        $payoutsData['errorMessage'] = $errorMessage;
        $payoutsData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();
        $payouts = DB::table('payouts')
            ->join('users', 'users.id', '=', 'payouts.user_id')
            ->paginate(40);
        $customers = DB::table('payouts')
            ->join('users', 'users.id', '=', 'payouts.user_id')
            ->groupBy('users.id')
            ->get();
        $payoutsData['payouts'] = $payouts;
        $payoutsData['customers'] = $customers;
        return view("admin.Orders.payouts", $title)->with('listingOrders', $payoutsData)->with('result', $result);
    }


    public
    function customerPayouts(Request $request)
    {
        $filter = $request->user;
        $title = array('pageTitle' => Lang::get("labels.ListingOrders"));
        $message = array();
        $errorMessage = array();
        $payoutsData['orders'] = $this->Order->paginator();
        $payoutsData['message'] = $message;
        $payoutsData['errorMessage'] = $errorMessage;
        $payoutsData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();
        $payouts = DB::table('payouts')
            ->join('users', 'users.id', '=', 'payouts.user_id')
            ->paginate(40);
        $customers = DB::table('payouts')
            ->join('users', 'users.id', '=', 'payouts.user_id')
            ->groupBy('users.id')
            ->get();
        if ($filter)
            $users = User::sortable(['id' => 'DESC'])
                ->where('id', $filter)
                ->where('role_id', 2)
                ->paginate(40);
        else
            $users = User::sortable(['id' => 'DESC'])
                ->where('role_id', 2)
                ->paginate(40);

        $users_list = User::sortable(['id' => 'DESC'])
            ->where('role_id', 2)
            ->get();
        foreach ($users as $each) {
            $total_earnings = 0;
            $total_approved = 0;
            $earnings = DB::table('orders')
                ->where('customers_id', $each->id)
                ->get();

            $amount = DB::table('payouts')
                ->where('user_id', $each->id)
                ->sum('amount');
            foreach ($earnings as $e) {

                $status = DB::table('orders_status_history')
                    ->where('orders_id', $e->orders_id)
                    ->orderBy('orders_status_history_id', 'desc')
                    ->first();


                if ($status->orders_status_id == 1 || $status->orders_status_id == 2 || $status->orders_status_id == 5 || $status->orders_status_id == 6) {
                    $total_earnings += $e->commission;
                }
                if ($status->orders_status_id == 2) {
                    $total_approved += $e->commission;
                }
            }

            $each['total_earnings'] = $total_earnings ? $total_earnings : 0;
            $each['total_approved'] = $total_approved ? $total_approved - $amount : 0;
        }
        $payoutsData['users'] = $users;
        $payoutsData['payouts'] = $payouts;
        $payoutsData['customers'] = $customers;

        return view("admin.Orders.customerPayouts", $title)->with('listingOrders', $payoutsData)
            ->with('result', $result)->with('filter', $filter)->with('users', $users_list);
    }


    public
    function addPayout($id)
    {
        $title = array('pageTitle' => Lang::get("labels.ListingOrders"));
        $message = array();
        $errorMessage = array();
        $payoutsData['orders'] = $this->Order->paginator();
        $payoutsData['message'] = $message;
        $payoutsData['errorMessage'] = $errorMessage;
        $payoutsData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();

        $users = User::where('role_id', 2)
            ->get();
        foreach ($users as $each) {
            $total_earnings = 0;
            $total_approved = 0;
            $earnings = DB::table('orders')
                ->where('customers_id', $each->id)
                ->get();

            $amount = DB::table('payouts')
                ->where('user_id', $each->id)
                ->sum('amount');
            foreach ($earnings as $e) {

                $status = DB::table('orders_status_history')
                    ->where('orders_id', $e->orders_id)
                    ->orderBy('orders_status_history_id', 'desc')
                    ->first();


                if ($status->orders_status_id == 1 || $status->orders_status_id == 2 || $status->orders_status_id == 5 || $status->orders_status_id == 6) {
                    $total_earnings += $e->commission;
                }
                if ($status->orders_status_id == 2) {
                    $total_approved += $e->commission;
                }
            }

            $each['total_earnings'] = $total_earnings ? $total_earnings : 0;
            $each['total_approved'] = $total_approved ? $total_approved - $amount : 0;

        }
        $result['users'] = $users;
        $result['id'] = $id;
        return view("admin.Orders.addPayout", $title)->with('listingOrders', $payoutsData)->with('result', $result);
    }

    public
    function getpayout(Request $request)
    {

        $message = array();
        $errorMessage = array();
        $payoutsData['orders'] = $this->Order->paginator();
        $payoutsData['message'] = $message;
        $payoutsData['errorMessage'] = $errorMessage;
        $payoutsData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();
        $total_earnings = 0;
        $total_approved = 0;
        $earnings = DB::table('orders')
            ->where('customers_id', $request->id)
            ->get();

        $amount = DB::table('payouts')
            ->where('user_id', $request->id)
            ->sum('amount');

        foreach ($earnings as $e) {

            $status = DB::table('orders_status_history')
                ->where('orders_id', $e->orders_id)
                ->orderBy('orders_status_history_id', 'desc')
                ->first();

//            Cancel=3;Return=4;Shipped=6;
//            Pending=1;Completed=2;Processing=5;
            if ($status->orders_status_id == 1 || $status->orders_status_id == 2 || $status->orders_status_id == 5 || $status->orders_status_id == 6) {
                $total_earnings += $e->commission;
            }
//            Completed=2;
            if ($status->orders_status_id == 2) {
                $total_approved += $e->commission;
            }
        }

        $each['total_earnings'] = $total_earnings ? $total_earnings - $amount : 0;
        $each['total_approved'] = $total_approved ? $total_approved - $amount : 0;

        $bankDetails = DB::table('bank_account_details')
            ->where('user_id', $request->id)
            ->first();
//

        $bankDetailsData = '';
        if ($bankDetails) {
            $bankDetailsData = '<div >
<div class="form-group">
<label for="name" class="col-sm-2 col-md-3 control-label"><b> Bank Name:</b></label><div class="col-sm-10 col-md-9">' . $bankDetails->bank_name . '</div></div>
<div class="form-group">
<label for="name" class="col-sm-2 col-md-3 control-label"><b> Account Holder Name:</b></label><div class="col-sm-10 col-md-9">' . $bankDetails->account_holder_name . '</div></div>
<div class="form-group">
<label for="name" class="col-sm-2 col-md-3 control-label"><b> Account Number:</b></label><div class="col-sm-10 col-md-9">' . $bankDetails->account_number . '</div></div>
<div class="form-group">
<label for="name" class="col-sm-2 col-md-3 control-label"><b> IFSC Code:</b></label><div class="col-sm-10 col-md-9">' . $bankDetails->ifsc_code . '</div></div>

<div class="form-group">
<label for="name" class="col-sm-2 col-md-3 control-label"><b> Phone Number:</b></label><div class="col-sm-10 col-md-9">' . $bankDetails->phone_number . '</div>
</div>';

        }


        $each['bank_details'] = $bankDetailsData;
        return $each;
    }

    public
    function getCustomerPayout($id)
    {
        $total_earnings = 0;
        $total_approved = 0;
        $earnings = DB::table('orders')
            ->where('customers_id', $id)
            ->get();
        $amount = DB::table('payouts')
            ->where('user_id', $id)
            ->sum('amount');
        foreach ($earnings as $e) {
            $status = DB::table('orders_status_history')
                ->where('orders_id', $e->orders_id)
                ->orderBy('orders_status_history_id', 'desc')
                ->first();
            if ($status->orders_status_id == 1 || $status->orders_status_id == 2 || $status->orders_status_id == 5 || $status->orders_status_id == 6) {
                $total_earnings += $e->commission;
            }
            if ($status->orders_status_id == 2) {
                $total_approved += $e->commission;
            }
        }

        $payout['total_earnings'] = $total_earnings ? $total_earnings : 0;
        $payout['total_approved'] = $total_approved ? $total_approved - $amount : 0;
        return $payout;

    }

    public
    function savePayout(Request $request)
    {
        DB::table('payouts')->insert(['user_id' => $request->user, 'amount' => $request->amount, 'created_at' => now()]);
        return redirect()->to('admin/payouts');
    }

    public
    function updatePayout(Request $request)
    {

    }

    public
    function editPayout($id)
    {
        $title = array('pageTitle' => Lang::get("labels.ListingOrders"));
        $message = array();
        $errorMessage = array();
        $payoutsData['orders'] = $this->Order->paginator();
        $payoutsData['message'] = $message;
        $payoutsData['errorMessage'] = $errorMessage;
        $payoutsData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();

        $users = User::where('role_id', 2)
            ->get();
        foreach ($users as $each) {
            $each['amounts'] = 100;
        }
        $result['users'] = $users;

        return view("admin.Orders.addPayout", $title)->with('listingOrders', $payoutsData)->with('result', $result);

    }

    public function vendorOrders(Request $request)
    {

        $start_date = $request->all() ? $request->start_date : date("d/m/Y");
        $end_date = $request->all() ? $request->end_date : date("d/m/Y");

        $vendor = $request->all() ? $request->vendor : 0;
        $title = array('pageTitle' => Lang::get("labels.ListingOrders"));
        $message = array();
        $ordersData['manufacturers'] = DB::table('manufacturers')->get();
        $errorMessage = array();
        $filter = $request->all() ? true : false;
        $ordersData['orders'] = $this->Order->vendorPaginator($request);

        $ordersData['message'] = $message;
        $ordersData['filter'] = $filter;
        $ordersData['vendor'] = $vendor;
        $ordersData['start_date'] = $start_date;
        $ordersData['end_date'] = $end_date;
        $ordersData['errorMessage'] = $errorMessage;
        $ordersData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.Orders.vendor", $title)->with('listingOrders', $ordersData)->with('result', $result);
    }

}
