<?php

namespace App\Http\Controllers\AdminControllers;

use App\Models\Core\Countries;
use App\Models\Core\Setting;
use App\Models\Core\Taluk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class TalukController extends Controller
{
    //
    public function __construct(Taluk $Taluk, Setting $setting)
    {
        $this->Taluk = $Taluk;
        $this->Setting = $setting;
    }

    public function index(Request $request)
    {
        $title = array('pageTitle' => Lang::get("labels.ListingZones"));
        $taluks = $this->Taluk->paginator();
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.taluk.index", $title)->with('result', $result)->with('taluks', $taluks);
    }

    public function add(Request $request)
    {
        $title = array('pageTitle' => Lang::get("labels.AddZone"));
        $result = array();
        $message = array();
        $districts = DB::table('districts')->get();
        $result['districts'] = $districts;
        $result['message'] = $message;
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.taluk.add", $title)->with('result', $result);
    }
    public function insert(Request $request)
    {
        $this->Taluk->insert($request);
        $message = Lang::get("labels.TalukAddedMessage");
        return Redirect::back()->with('message', $message);
    }

    public function edit(Request $request)
    {
        $title = array('pageTitle' => Lang::get("labels.EditZone"));
        $result = array();
        $result['message'] = array();
        $taluks = $this->Taluk->edit($request);
        $districts = DB::table('districts')->get();
        $result['districts'] = $districts;
        $result['taluk'] = $taluks;
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.taluk.edit", $title)->with('result', $result);
    }
    public function update(Request $request)
    {
        $this->Taluk->updaterecord($request);
        $countryData['message'] = 'Taluk has been updated successfully!';
        $message = Lang::get("labels.Taluk has been updated successfully");
        return Redirect::back()->with('message', $message);
    }

    public function delete(Request $request)
    {
        $this->Taluk->deleterecord($request);
        return redirect()->back()->withErrors([Lang::get("labels.ZoneDeletedTax")]);
    }

    public function filter(Request $request)
    {
        $name = $request->FilterBy;
        $param = $request->parameter;
        $title = array('pageTitle' => Lang::get("labels.ListingTaluk"));
        $taluks = $this->Taluk->filter($request);
        $result['commonContent'] = $this->Setting->commonContent();
        return view("admin.taluk.index", $title)->with('result', $result)->with('taluks', $taluks)->with('name', $name)->with('param', $param);
    }


}
