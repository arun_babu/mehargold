<?php

namespace App\Http\Controllers\AdminControllers;

use App\Models\Core\Order;
use App\Models\Core\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class UserManagementController extends Controller
{
    public function __construct(Setting $setting, Order $order)
    {
        $this->myVarsetting = new SiteSettingController($setting);
        $this->Setting = $setting;
        $this->Order = $order;
    }

    //
    public function users()
    {
        $title = array('pageTitle' => Lang::get("labels.ListingOrders"));
        $message = array();
        $errorMessage = array();
        $payoutsData['orders'] = $this->Order->paginator();
        $payoutsData['message'] = $message;
        $payoutsData['errorMessage'] = $errorMessage;
        $payoutsData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();
        $payoutsData['users'] = DB::table('users')
            ->orderBy('id','DESC')
            ->paginate(40);
//            ->get();
        $customers = DB::table('payouts')
            ->join('users', 'users.id', '=', 'payouts.user_id')
            ->groupBy('users.id')
            ->get();
        $payoutsData['customers'] = $customers;
        return view("admin.user.index", $title)->with('listingOrders', $payoutsData)->with('result', $result);

    }

    public function user($id)
    {
        $title = array('pageTitle' => Lang::get("labels.ListingOrders"));
        $message = array();
        $errorMessage = array();
        $payoutsData['orders'] = $this->Order->paginator();
        $payoutsData['message'] = $message;
        $payoutsData['errorMessage'] = $errorMessage;
        $payoutsData['currency'] = $this->myVarsetting->getSetting();
        $result['commonContent'] = $this->Setting->commonContent();
        $user = DB::table('users')
            ->where('users.id', $id)
            ->first();


        $profile_details = DB::table('profile_details')
            ->where('profile_details.user_id', $id)
            ->join('countries', 'countries.countries_id', '=', 'profile_details.country')
            ->join('districts', 'districts.id', '=', 'profile_details.district')
            ->join('states', 'states.id', '=', 'profile_details.state')
            ->join('taluk', 'taluk.id', '=', 'profile_details.taluk')
            ->first();

        $profile_docs = DB::table('profile_details')
            ->where('profile_details.user_id', $id)
            ->first();

        $user->address = $profile_details ? $profile_details->address_line_one . "\n" . $profile_details->address_line_two . "\n" . $profile_details->pin : '';
        $user->country = $profile_details ? $profile_details->countries_name : '';
        $user->state = $profile_details ? $profile_details->state : '';
        $user->district = $profile_details ? $profile_details->district : '';
        $user->taluk = $profile_details ? $profile_details->taluk : '';
        $bankdetais=DB::table('bank_account_details')
            ->where('user_id',$user->id)
            ->get();

        $doc = 'Link';
        $doc_link = '';

        if ($profile_docs) {
            $docExplode = explode('/', $profile_docs->doc);
            $doc = $docExplode[count($docExplode) - 1];
            $doc_link = $profile_docs->doc;
        }
        $user->doc = $doc;
        $user->doc_link = $doc_link;
        return view("admin.user.view", $title)->with('listingOrders', $payoutsData)->with('result', $result)->with('user', $user)->with('bankdetais', $bankdetais);
    }

    public function status(Request $request)
    {
        DB::table('users')->where('id', $request->id)
            ->update(['status' => $request->status,'status_description'=>$request->comments]);
        $user = DB::table('users')->where('id', $request->id)->first();

        return redirect('/admin/usermanagement/'.$request->id);
    }


}
