<?php

namespace App\Http\Controllers\Web;

use App\Models\Web\Currency;
use App\Models\Web\Customer;
use App\Models\Web\Index;
use App\Models\Web\Languages;
use App\Models\Web\Products;
use Authy\AuthyApi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Web\Cart;
use DB;

use Lang;


class BankdetailsController extends Controller
{
    public function __construct(
        Index $index,
        Languages $languages,
        Products $products,
        Currency $currency,
        Customer $customer,
        Cart $cart
    )
    {
        $this->index = $index;
        $this->languages = $languages;
        $this->products = $products;
        $this->currencies = $currency;
        $this->customer = $customer;
        $this->cart = $cart;
        $this->theme = new ThemeController();

        $this->authy = new AuthyApi(config('app.twilio')['AUTHY_API_KEY']);
        // Twilio credentials
        $this->sid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
        $this->authToken = config('app.twilio')['TWILIO_AUTH_TOKEN'];
        $this->twilioFrom = config('app.twilio')['TWILIO_PHONE'];
    }

    //
    public function Index()
    {
        $title = array('pageTitle' => Lang::get("website.Profile"));
        $result['commonContent'] = $this->index->commonContent();
        $final_theme = $this->theme->theme();

        $id = auth()->guard('customer')->user()->id;

        $bankDetails = DB::table('bank_account_details')->where('user_id', $id)->get();


        return view('web.bank-details.index', ['result' => $result, 'title' => $title, 'final_theme' => $final_theme, 'bankDetails' => $bankDetails]);
    }

    public function create()
    {
        $result['commonContent'] = $this->index->commonContent();
        $final_theme = $this->theme->theme();
        return view('web.bank-details.create', ['result' => $result, 'final_theme' => $final_theme]);
    }

    public function store(Request $request)
    {
        $getData = DB::table('bank_account_details')
            ->where('user_id', auth()->guard('customer')->user()->id)
            ->where('active', 1)
            ->get();
        if ($request->active == 1)
            if (count($getData)) {
                foreach ($getData as $each) {
                    DB::table('bank_account_details')
                        ->where('id', $each->id)
                        ->update(['active' => 0]);
                }
            }

        DB::table('bank_account_details')
            ->insert([
                'bank_name' => $request->bank_name,
                'account_number' => $request->account_number,
                'ifsc_code' => $request->ifsc_code,
                'phone_number' => $request->phone_number,
                'active' => $request->active,
                'user_id' => auth()->guard('customer')->user()->id,
            ]);
        return redirect('bank-details');

    }

    public function edit($id)
    {
        $result['commonContent'] = $this->index->commonContent();
        $final_theme = $this->theme->theme();
        $bankDetails = DB::table('bank_account_details')
            ->where('id', $id)
            ->first();
        return view('web.bank-details.edit', ['result' => $result, 'final_theme' => $final_theme, 'bankDetails' => $bankDetails]);
    }

    public function update(Request $request)
    {


        $getData = DB::table('bank_account_details')
            ->where('user_id', auth()->guard('customer')->user()->id)
            ->get();
        foreach ($getData as $each) {
            if ($each->id == $request->id) {
                $request->active = $each->active == 1 ? 1 : $request->active;
            }
            DB::table('bank_account_details')
                ->where('id', $each->id)
                ->update(['active' => 0]);
        }
        DB::table('bank_account_details')
            ->where('id', $request->id)
            ->update([
                'bank_name' => $request->bank_name,
                'account_number' => $request->account_number,
                'ifsc_code' => $request->ifsc_code,
                'phone_number' => $request->phone_number,
                'active' => $request->active,
                'user_id' => auth()->guard('customer')->user()->id,
            ]);

        return redirect('bank-details');
    }

    public function delete($id)
    {
        DB::table('bank_account_details')
            ->where('id', $id)
            ->delete();
        return redirect('bank-details');

    }


}
