<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Web\AlertController;
use App\Models\Web\Cart;
use App\Models\Web\Currency;
use App\Models\Web\Customer;
use App\Models\Web\Index;
use App\Models\Web\Languages;
use App\Models\Web\Products;
use App\User;
use Auth;
use Authy\AuthyApi;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Lang;
use Session;
use Socialite;
use Twilio\Rest\Client;
use Validator;
use Hash;
use Storage;


class CustomersController extends Controller
{

    public function __construct(
        Index $index,
        Languages $languages,
        Products $products,
        Currency $currency,
        Customer $customer,
        Cart $cart
    )
    {
        $this->index = $index;
        $this->languages = $languages;
        $this->products = $products;
        $this->currencies = $currency;
        $this->customer = $customer;
        $this->cart = $cart;
        $this->theme = new ThemeController();

        $this->authy = new AuthyApi(config('app.twilio')['AUTHY_API_KEY']);
        // Twilio credentials
        $this->sid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
        $this->authToken = config('app.twilio')['TWILIO_AUTH_TOKEN'];
        $this->twilioFrom = config('app.twilio')['TWILIO_PHONE'];
    }

    public function signup(Request $request)
    {
        $final_theme = $this->theme->theme();
        if (auth()->guard('customer')->check()) {
            return redirect('/');
        } else {
            $title = array('pageTitle' => Lang::get("website.Sign Up"));
            $result = array();
            $result['commonContent'] = $this->index->commonContent();
            return view("auth.signup_new", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
        }
    }

    public function validate(Request $request)
    {
        $validator = Validator::make(
            array(
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'customers_gender' => $request->gender,
                'email' => $request->email,
                'password' => $request->password,
                're_password' => $request->re_password,
                'phone' => $request->phone,

            ), array(
                'firstName' => 'required ',
                'lastName' => 'required',
                'customers_gender' => 'required',
                'email' => 'required | email|unique:users',
                'password' => 'required',
                're_password' => 'required | same:password',
                'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users',
            )
        );
        $user_email = \Illuminate\Support\Facades\DB::table('users')->select('email')->where('role_id', 2)->where('email', $request->email)->get();
        $user_phone = DB::table('users')->select('phone')->where('role_id', 2)->where('phone', $request->phone)->get();
        if ($validator->fails()) {

            return response()->json(['error' => $validator->errors()->all()]);
        }
        return response()->json(['error' => 'null']);

    }

    public function verify(Request $request)
    {
        $response = $this->authy->phoneVerificationStart($request->phone, +91, 'sms');

        return response()->json(['status' => $response->ok()]);
    }

    public function confirm(Request $request)
    {

        $response = $this->authy->phoneVerificationCheck($request->phone, +91, $request->verification_code);

        /* Get credentials from .env */
//        $token = getenv("TWILIO_AUTH_TOKEN");
//        $twilio_sid = getenv("TWILIO_SID");
//        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
//        $twilio = new Client($twilio_sid, $token);
//        $verification = $twilio->verify->v2->services($twilio_verify_sid)
//            ->verificationChecks
//            ->create($request->verification_code, array('to' => $request->phone));
        return response()->json(['status' => $response->ok()]);

//        if ($verification->valid) {
//            $user = tap(User::where('phone_number', $data['phone_number']))->update(['isVerified' => true]);
//            /* Authenticate user */
//            Auth::login($user->first());
//            return redirect()->route('home')->with(['message' => 'Phone number verified']);
//        }
//        return back()->with(['phone_number' => $data['phone_number'], 'error' => 'Invalid verification code entered!']);

    }

    public function login(Request $request)
    {
        $result = array();
        if (auth()->guard('customer')->check()) {
            if (false) {
                return view("auth.login_new");
            } else {
                return redirect('/');
            }


        } else {
            $result['cart'] = $this->cart->myCart($result);

            if (count($result['cart']) != 0) {
                $result['checkout_button'] = 1;
            } else {
                $result['checkout_button'] = 0;

            }
            $previous_url = Session::get('_previous.url');

            $ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
            $ref = rtrim($ref, '/');

            session(['previous' => $previous_url]);

            $title = array('pageTitle' => Lang::get("website.Login"));
            $final_theme = $this->theme->theme();

            $result['commonContent'] = $this->index->commonContent();
//            return view("auth.login", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);

            return view("auth.login_new", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
        }

    }

    public function processLogin(Request $request)
    {
        $old_session = Session::getId();

        $result = array();

        //check authentication of email and password
        $phone = false;
        $email = false;
        if ($request->phone) {
            if (strpos($request->phone, '@')) {
                $email = $request->phone;
            } else {
                $phone = $request->phone;
            }
        } elseif ($request->email) {
            if (strpos($request->email, '@')) {
                $email = $request->email;
            } else {
                $phone = $request->email;
            }
        }
        $password = $request->password;
        if ($phone)
            $customerInfo = array("phone" => $phone, "password" => $password, 'role_id' => 2);
        else
            $customerInfo = array("email" => $email, "password" => $password, 'role_id' => 2);


//        $customerInfo = array("email" => $request->email, "password" => $request->password);
//        $customerInfo = array("phone" => $request->phone, "password" => $request->password);

        if (auth()->guard('customer')->attempt($customerInfo)) {
            $customer = auth()->guard('customer')->user();
            if ($customer->role_id != 2) {
                $record = DB::table('settings')->where('id', 94)->first();
                if ($record->value == 'Maintenance' && $customer->role_id == 1) {
                    auth()->attempt($customerInfo);
                } else {
                    Auth::guard('customer')->logout();
                    return redirect('login')->with('loginError', Lang::get("website.You Are Not Allowed With These Credentials!"));
                }
            }
            $result = $this->customer->processLogin($request, $old_session);

            $id = auth()->guard('customer')->user()->id;
            $profile_details = DB::table('profile_details')->where('user_id', $id)->get();
            $profile_address = DB::table('profile_details')
                ->where('user_id', $id)
                ->where('address_line_one', '!=', '')
                ->get();
            $bank_account_details = DB::table('bank_account_details')->where('user_id', $id)->get();


            if (!empty(session('previous'))) {
                return Redirect::to(session('previous'));
            } else {
                Session::forget('guest_checkout');
                return redirect()->intended('/')->with('result', $result);
            }
        } else {
            return redirect('login')->with('loginError', Lang::get("website.Email or password is incorrect"));
        }
        //}
    }

    public function addToCompare(Request $request)
    {
        $cartResponse = $this->customer->addToCompare($request);
        return $cartResponse;
    }

    public function DeleteCompare($id)
    {
        $Response = $this->customer->DeleteCompare($id);
        return redirect()->back()->with($Response);
    }

    public function Compare()
    {
        $result = array();
        $final_theme = $this->theme->theme();
        $result['commonContent'] = $this->index->commonContent();
        $compare = $this->customer->Compare();
        $results = array();
        foreach ($compare as $com) {
            $data = array('products_id' => $com->product_ids, 'page_number' => '0', 'type' => 'compare', 'limit' => '15', 'min_price' => '', 'max_price' => '');
            $newest_products = $this->products->products($data);
            array_push($results, $newest_products);
        }
        $result['products'] = $results;
        return view('web.compare', ['result' => $result, 'final_theme' => $final_theme]);
    }

    public function profile()
    {
        $title = array('pageTitle' => Lang::get("website.Profile"));
        $result['commonContent'] = $this->index->commonContent();
        $final_theme = $this->theme->theme();
        return view('web.profile', ['result' => $result, 'title' => $title, 'final_theme' => $final_theme]);
    }

//added by dev
    public function profileDetails()
    {
        $states = [];
        $districts = [];
        $taluks = [];
        $title = array('pageTitle' => Lang::get("website.Profile"));
        $result['commonContent'] = $this->index->commonContent();
        $final_theme = $this->theme->theme();
        $profileDetails = DB::table('profile_details')->where('user_id', auth()->guard('customer')->user()->id)->first();
        $countries = DB::table('countries')->get();
        if ($profileDetails) {
            $states = DB::table('states')->where('country_id', $profileDetails->country)->get();
            $districts = DB::table('districts')->where('state_id', $profileDetails->state)->get();
            $taluks = DB::table('taluk')->where('district_id', $profileDetails->district)->get();
        }
        return view('web.profile-details', ['result' => $result, 'title' => $title, 'final_theme' => $final_theme, 'profileDetails' => $profileDetails, 'countries' => $countries, 'states' => $states, 'districts' => $districts, 'taluks' => $taluks]);
    }

//added by dev
    public function documents()
    {
        $states = [];
        $districts = [];
        $taluks = [];
        $title = array('pageTitle' => Lang::get("website.Profile"));
        $result['commonContent'] = $this->index->commonContent();
        $final_theme = $this->theme->theme();
        $profileDetails = DB::table('profile_details')->where('user_id', auth()->guard('customer')->user()->id)->first();
        $countries = DB::table('countries')->get();
        if ($profileDetails) {
            $states = DB::table('states')->where('country_id', $profileDetails->country)->get();
            $districts = DB::table('districts')->where('state_id', $profileDetails->state)->get();
            $taluks = DB::table('taluk')->where('district_id', $profileDetails->district)->get();
        }
        return view('web.documents', ['result' => $result, 'title' => $title, 'final_theme' => $final_theme, 'profileDetails' => $profileDetails, 'countries' => $countries, 'states' => $states, 'districts' => $districts, 'taluks' => $taluks]);
    }

    public function getStates($id)
    {
        $states = DB::table('zones')->where('zone_country_id', $id)->get();

        return $states;
    }

    public function getDistricts($id)
    {
        $districts = DB::table('districts')->where('state_id', $id)->get();
        return $districts;
    }

    public function getTaluk($id)
    {
        $taluks = DB::table('taluk')->where('district_id', $id)->get();
        return $taluks;
    }

    public function earnings()
    {
        $total_approved = 0;
        $total_earnings = 0;
        $title = array('pageTitle' => Lang::get("website.Profile"));
        $result['commonContent'] = $this->index->commonContent();
        $final_theme = $this->theme->theme();

        $user = auth()->guard('customer')->user();

        $amount = DB::table('payouts')
            ->where('user_id', $user->id)
            ->sum('amount');
        $earnings = DB::table('orders')
            ->where('customers_id', $user->id)
            ->get();
        foreach ($earnings as $e) {
            $status = DB::table('orders_status_history')
                ->where('orders_id', $e->orders_id)
                ->orderBy('orders_status_history_id', 'desc')
                ->first();
            if ($status->orders_status_id == 1 || $status->orders_status_id == 2 || $status->orders_status_id == 5) {
                $total_earnings += $e->commission;
            }
            if ($status->orders_status_id == 2) {
                $total_approved += $e->commission;
            }
        }

        return view('web.earnings', ['result' => $result, 'title' => $title, 'final_theme' => $final_theme, 'total_approved' => $total_approved - $amount, 'total_earnings' => $total_earnings]);
    }

    public function updateMyProfile(Request $request)
    {
        $message = $this->customer->updateMyProfile($request);
        return redirect()->back()->with('success', $message);

    }

//added by dev
    public function updateMyProfileDetails(Request $request)
    {

        $photo = '';
        $doc = '';
        if ($request->photo) {
            $imageName = time() . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('photos'), $imageName);
            $photo = 'photos/' . $imageName;
        }
        if ($request->doc) {
            $docName = time() . '.' . $request->doc->getClientOriginalExtension();
            $request->doc->move(public_path('docs'), $docName);
            $doc = 'docs/' . $docName;
        }
        if ($request->id) {
            if ($photo && $doc) {
                DB::table('profile_details')
                    ->where('id', $request->id)
                    ->update(['user_id' => $request->userId, 'address_line_one' => $request->addrssLine1, 'address_line_two' => $request->addrssLine2, 'pin' => $request->pin, 'country' => $request->country, 'district' => $request->district, 'state' => $request->state, 'taluk' => $request->taluk, 'doc' => $doc, 'photo' => $photo]);
            } else {
                if ($photo)
                    DB::table('profile_details')
                        ->where('id', $request->id)
                        ->update(['user_id' => $request->userId, 'address_line_one' => $request->addrssLine1, 'address_line_two' => $request->addrssLine2, 'pin' => $request->pin, 'country' => $request->country, 'district' => $request->district, 'state' => $request->state, 'taluk' => $request->taluk, 'photo' => $photo]);
                if ($doc)
                    DB::table('profile_details')
                        ->where('id', $request->id)
                        ->update(['user_id' => $request->userId, 'address_line_one' => $request->addrssLine1, 'address_line_two' => $request->addrssLine2, 'pin' => $request->pin, 'country' => $request->country, 'district' => $request->district, 'state' => $request->state, 'taluk' => $request->taluk, 'doc' => $doc]);

                if (!$photo && !$doc) {
                    DB::table('profile_details')
                        ->where('id', $request->id)
                        ->update(['user_id' => $request->userId, 'address_line_one' => $request->addrssLine1, 'address_line_two' => $request->addrssLine2, 'pin' => $request->pin, 'country' => $request->country, 'district' => $request->district, 'state' => $request->state, 'taluk' => $request->taluk]);
                }
            }
        } else {
            DB::table('profile_details')
                ->insert(
                    ['user_id' => $request->userId,
                        'address_line_one' => $request->addrssLine1,
                        'address_line_two' => $request->addrssLine2,
                        'pin' => $request->pin,
                        'country' => $request->country,
                        'district' => $request->district,
                        'state' => $request->state,
                        'taluk' => $request->taluk,
                        'doc' => $doc,
                        'photo' => $photo]);
        }

        $message = Lang::get("website.Profile has been updated successfully");
        if (session('redirection') != '') {
            session(['redirection' => '']);
            return redirect('checkout');
        }


        return redirect()->back()->with('success', $message);
    }

    public function addMyProfileDetails(Request $request)
    {
        if ($request->addrssLine1 && $request->country && $request->district && $request->state && $request->taluk) {
            $id = DB::table('profile_details')
                ->insertGetId(
                    ['user_id' => auth()->guard('customer')->user()->id,
                        'address_line_one' => $request->addrssLine1,
                        'address_line_two' => $request->addrssLine2,
                        'pin' => $request->pin,
                        'country' => $request->country,
                        'district' => $request->district,
                        'state' => $request->state,
                        'taluk' => $request->taluk,
                    ]);
            if ($id) {
//                return redirect('/addDocuments');
//            } else {
                $message = Lang::get("Something went wrong");
                return redirect('/addContactInfo')->with('error', $message);

            }
        } else {
            $message = Lang::get("Some fields are missing");
            return redirect('/addContactInfo')->with('error', $message);

        }

    }

    public function addDocument(Request $request)
    {
        $id = auth()->guard('customer')->user()->id;
        $photo = 0;
        $doc = 0;
        if ($request->photo) {
            $imageName = time() . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('photos'), $imageName);
            $photo = 'photos/' . $imageName;
        }
        if ($request->doc) {
            $docName = time() . '.' . $request->doc->getClientOriginalExtension();
            $request->doc->move(public_path('docs'), $docName);
            $doc = 'docs/' . $docName;
        }

        if ($photo) {
            DB::table('profile_details')
                ->where('user_id', $id)
                ->update(['photo' => $photo]);
            DB::table('users')
                ->where('id', $id)
                ->update(['avatar' => $photo]);
        }

        if ($doc)
            DB::table('profile_details')
                ->where('user_id', $id)
                ->update(['doc' => $doc]);
        if ($photo) {
            return redirect('/addBankDetails');
        } else {
            $message = Lang::get("Something went wrong");
            return redirect('/addDocuments')->with('error', $message);
        }


    }

    public function addBankDetails(Request $request)
    {
        if ($request->accno != $request->accno_sec) {
            $message = Lang::get("Account Number Not Match");
            return redirect('/addBankDetail')->with('error', $message);
        } elseif ($request->name && $request->accno && $request->ifsc) {

            $id = auth()->guard('customer')->user()->id;
            $account_holder_name = $request->name;
            $account_number = $request->accno;
            $ifsc_code = $request->ifsc;
            $active = $request->active ? $request->active : 1;

            $getAcount = DB::table('bank_account_details')
                ->where('account_number', $account_number)
                ->get();

            if (count($getAcount) > 0) {
                $message = Lang::get("Account number already exist!");
                return redirect()->back()->with('error', $message);

            } else {
                $getData = DB::table('bank_account_details')
                    ->where('user_id', $id)
                    ->where('active', 1)
                    ->get();

                if ($active == 1)
                    if (count($getData)) {
                        foreach ($getData as $each) {
                            DB::table('bank_account_details')
                                ->where('id', $each->id)
                                ->update(['active' => 0]);
                        }
                    }
                $account = DB::table('bank_account_details')
                    ->insert([
//                'bank_name' => $request->bank_name,
                        'account_number' => $account_number,
                        'ifsc_code' => $ifsc_code,
                        'account_holder_name' => $account_holder_name,
                        'active' => $active,
                        'user_id' => $id,
                    ]);

            }
            $message = Lang::get("website.Profile has been updated successfully");
            return redirect('/');
        } else {
            $message = Lang::get("Something went wrong");
            return redirect('/addBankDetail')->with('error', $message);
        }

    }


    //added by dev
    public function updateDocuments(Request $request)
    {

        $photo = '';
        $doc = '';

        if ($request->photo) {
            $imageName = 'images/profile/' . time() . '.' . $request->photo->getClientOriginalExtension();
            $image = $request->file('photo');
            Storage::disk('s3')->put($imageName, file_get_contents($image), 'public');
            $photo = $imageName;
        }
        if ($request->doc) {
            $docName = 'docs/profile/' . time() . '.' . $request->doc->getClientOriginalExtension();
            $docs = $request->file('doc');
            Storage::disk('s3')->put($docName, file_get_contents($docs), 'public');
            $doc = $docName;
        }
        if ($request->id) {
            if ($photo && $doc) {
                DB::table('profile_details')
                    ->where('id', $request->id)
                    ->update(['user_id' => $request->userId, 'doc' => $doc, 'photo' => $photo]);
                DB::table('users')
                    ->where('id', auth()->guard('customer')->user()->id)
                    ->update(['avatar' => $photo]);

            } else {
                if ($photo) {
                    DB::table('profile_details')
                        ->where('id', $request->id)
                        ->update(['user_id' => $request->userId, 'photo' => $photo]);
                    DB::table('users')
                        ->where('id', auth()->guard('customer')->user()->id)
                        ->update(['avatar' => $photo]);
                }

                if ($doc) {
                    DB::table('profile_details')
                        ->where('id', $request->id)
                        ->update(['user_id' => $request->userId, 'doc' => $doc]);
                }

            }
        } else {
            DB::table('profile_details')->insert(['user_id' => $request->userId, 'doc' => $doc, 'photo' => $photo]);
        }
        $message = Lang::get("website.Profile has been updated successfully");
        $message = "Document has been updated successfully";
        if (session('redirection') != '') {
            session(['redirection' => '']);
            return redirect('checkout');
        }

        return redirect()->back()->with('success', $message);
    }

    public function changePassword()
    {
        $title = array('pageTitle' => Lang::get("website.Change Password"));
        $result['commonContent'] = $this->index->commonContent();
        $final_theme = $this->theme->theme();
        return view('web.changepassword', ['result' => $result, 'title' => $title, 'final_theme' => $final_theme]);
    }

    public function updateMyPassword(Request $request)
    {
        $password = Auth::guard('customer')->user()->password;
        if (Hash::check($request->current_password, $password)) {
            $message = $this->customer->updateMyPassword($request);
            return redirect()->back()->with('success', $message);
        } else {
            return redirect()->back()->with('error', lang::get("website.Current password is invalid"));
        }
    }

    public function logout(REQUEST $request)
    {
        Auth::guard('customer')->logout();
        session()->flush();
        $request->session()->forget('customers_id');
        $request->session()->regenerate();
        return redirect()->intended('/');
    }

    public function socialLogin($social)
    {
        return Socialite::driver($social)->redirect();
    }

    public function handleSocialLoginCallback($social)
    {
        $result = $this->customer->handleSocialLoginCallback($social);
        if (!empty($result)) {
            return redirect()->intended('/')->with('result', $result);
        }
    }

    public function createRandomPassword()
    {
        $pass = substr(md5(uniqid(mt_rand(), true)), 0, 8);
        return $pass;
    }

    public function likeMyProduct(Request $request)
    {
        $cartResponse = $this->customer->likeMyProduct($request);
        return $cartResponse;
    }

    public function unlikeMyProduct(Request $request, $id)
    {

        if (!empty(auth()->guard('customer')->user()->id)) {
            $this->customer->unlikeMyProduct($id);
            $message = Lang::get("website.Product is unliked");
            return redirect()->back()->with('success', $message);
        } else {
            return redirect('login')->with('loginError', 'Please login to like product!');
        }

    }

    public function wishlist(Request $request)
    {
        $title = array('pageTitle' => Lang::get("website.Wishlist"));
        $final_theme = $this->theme->theme();
        $result = $this->customer->wishlist($request);
        return view("web.wishlist", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
    }

    public function loadMoreWishlist(Request $request)
    {

        $limit = $request->limit;

        $data = array('page_number' => $request->page_number, 'type' => 'wishlist', 'limit' => $limit, 'categories_id' => '', 'search' => '', 'min_price' => '', 'max_price' => '');
        $products = $this->products->products($data);
        $result['products'] = $products;

        $cart = '';
        $myVar = new CartController();
        $result['cartArray'] = $this->products->cartIdArray($cart);
        $result['limit'] = $limit;
        return view("web.wishlistproducts")->with('result', $result);

    }

    public function forgotPassword()
    {
        if (auth()->guard('customer')->check()) {
            return redirect('/');
        } else {

            $title = array('pageTitle' => Lang::get("website.Forgot Password"));
            $final_theme = $this->theme->theme();
            $result = array();
            $result['commonContent'] = $this->index->commonContent();
            return view("web.forgotpassword_new", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
//            return view("web.forgotpassword", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
        }
    }

    public function addNewPassword(Request $request)
    {
//    check whether otp is valid
        $valid = true;
        $phone = $request->phone;

        if ($valid) {
            return view("web.addnewpassword")->with('phone', $phone);
        } else {

            return redirect('forgotPassword')->with('error', 'Invalid OTP!');
        }
//        check otp is right
        if (auth()->guard('customer')->check()) {
            return redirect('/');
        } else {
            $title = array('pageTitle' => Lang::get("website.Forgot Password"));
            $final_theme = $this->theme->theme();
            $result = array();
            $result['commonContent'] = $this->index->commonContent();
            return view("web.addnewpassword", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
//            return view("web.forgotpassword", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
        }
    }


    public function processPassword(Request $request)
    {
        //validate
        if ($request->new_password != $request->confirm_password)
            return redirect('forgotPassword')->with('error', 'Password Does not match');
        $phone = $request->phone;
        $getUser = DB::table('users')->where('phone', $phone)->first();
        if (!$getUser)
            return redirect('forgotPassword')->with('error', 'Phone Number Does not exist');
        $title = array('pageTitle' => Lang::get("website.Forgot Password"));
        $password = $this->createRandomPassword();
        //check email exist
        $existUser = $this->customer->ExistUser($phone);
        if (count($existUser) > 0) {
//            $this->customer->UpdateExistUser($phone, $password);
            //$myVar = new AlertController();
            //$alertSetting = $myVar->forgotPasswordAlert($existUser);

            return redirect('forgotPassword')->with('success', 'Password has been Updated');
        } else {
            return redirect('forgotPassword')->with('error', Lang::get("website.Email address does not exist"));
        }

    }

    public function recoverPassword()
    {
        $title = array('pageTitle' => Lang::get("website.Forgot Password"));
        $final_theme = $this->theme->theme();
        return view("web.recoverPassword", ['title' => $title, 'final_theme' => $final_theme])->with('result', $result);
    }

    public function subscribeNotification(Request $request)
    {

        $setting = $this->index->commonContent();

        /* Desktop */
        $type = 3;

        session(['device_id' => $request->device_id]);

        if (auth()->guard('customer')->check()) {

            $device_data = array(
                'device_id' => $request->device_id,
                'device_type' => $type,
                'created_at' => time(),
                'updated_at' => time(),
                'ram' => '',
                'status' => '1',
                'processor' => '',
                'device_os' => '',
                'location' => '',
                'device_model' => '',
                'user_id' => auth()->guard('customers')->user()->id,
                'manufacturer' => '',
            );

        } else {

            $device_data = array(
                'device_id' => $request->device_id,
                'device_type' => $type,
                'created_at' => time(),
                'updated_at' => time(),
                'ram' => '',
                'status' => '1',
                'processor' => '',
                'device_os' => '',
                'location' => '',
                'device_model' => '',
                'manufacturer' => '',
            );

        }
        $this->customer->updateDevice($request, $device_data);
        print 'success';
    }

    public function signupProcess(Request $request)
    {
        $old_session = Session::getId();
        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $gender = $request->gender;
        $email = $request->email;
        $password = $request->password;
        $password = $request->password;
        $phone = $request->phone;
        $date = date('y-md h:i:s');
        //        //validation start
        $validator = Validator::make(
            array(
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'customers_gender' => $request->gender,
                'email' => $request->email,
                'password' => $request->password,
                're_password' => $request->re_password,
                'phone' => $request->phone,

            ), array(
                'firstName' => 'required ',
                'lastName' => 'required',
                'customers_gender' => 'required',
                'email' => 'required | email',
                'password' => 'required',
                're_password' => 'required | same:password',
                'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            )
        );
        if ($validator->fails()) {
            return redirect('login')->withErrors($validator)->withInput();
        } else {
            $res = $this->customer->signupProcess($request);
            //eheck email already exit

            if ($res['email'] == "true") {
                return redirect('/login')->withInput($request->input())->with('error', Lang::get("website.Email already exist"));
            } else {
                if ($res['phone'] == "true") {
                    return redirect('/login')->withInput($request->input())->with('error', Lang::get("website.Phone already exist"));
                } else {
                    if ($res['insert'] == "true") {
                        if ($res['auth'] == "true") {
                            $result = $res['result'];
                            Session::forget('guest_checkout');
                            return redirect()->intended('/addContactInfo')->with('result', $result);
                        } else {
                            return redirect('login')->with('loginError', Lang::get("website.Email or password is incorrect"));
                        }
                    } else {
                        return redirect('/login')->with('error', Lang::get("website.something is wrong"));
                    }
                }
            }

        }

    }

//    complete profile details for further process
    public function addContactInfo(Request $request)
    {
        $id = auth()->guard('customer')->user()->id;
        $profile_details = DB::table('profile_details')->where('user_id', $id)->get();
        $profile_address = DB::table('profile_details')
            ->where('user_id', $id)
            ->where('address_line_one', '!=', '')
            ->get();
        $profile_poto = DB::table('profile_details')
            ->where('user_id', $id)
            ->where('photo', '!=', '')
            ->get();
        $bank_account_details = DB::table('bank_account_details')->where('user_id', $id)->get();
        $countries = DB::table('countries')->get();

//
        if (count($profile_address) == 0) {
//            return if not complete address
            return view("auth.contact", ['countries' => $countries]);
        }
//        elseif (count($profile_poto) == 0) {
////            retun if not upload documents
//            return redirect('/addDocuments');
//        }
        elseif (count($bank_account_details) == 0) {
//            return if not update bank details
            return redirect('/addBankDetail');
        } else {
            return redirect('/');
        }
    }

    public function addDocuments(Request $request)
    {
        $id = auth()->guard('customer')->user()->id;
        $profile_details = DB::table('profile_details')->where('user_id', $id)->get();
        $profile_address = DB::table('profile_details')
            ->where('user_id', $id)
            ->where('address_line_one', '!=', '')
            ->get();
        $profile_poto = DB::table('profile_details')
            ->where('user_id', $id)
            ->where('photo', '!=', '')
            ->get();
        $bank_account_details = DB::table('bank_account_details')->where('user_id', $id)->get();
        $countries = DB::table('countries')->get();

//
        if (count($profile_poto) == 0) {
//            retun if not upload documents
            return view("auth.document");
        } elseif (count($bank_account_details) == 0) {
//            return if not update bank details
            return redirect('/addBankDetail');
        } else {
            return redirect('/');
        }

    }

    public function addBankDetail(Request $request)
    {
        $id = auth()->guard('customer')->user()->id;
        $profile_details = DB::table('profile_details')->where('user_id', $id)->get();
        $profile_address = DB::table('profile_details')
            ->where('user_id', $id)
            ->where('address_line_one', '!=', '')
            ->get();
        $profile_poto = DB::table('profile_details')
            ->where('user_id', $id)
            ->where('photo', '!=', '')
            ->get();
        $bank_account_details = DB::table('bank_account_details')->where('user_id', $id)->get();
        $countries = DB::table('countries')->get();
//
        if (count($bank_account_details) == 0) {
//            return if not update bank details
            return view("auth.bank_details");
        } else {
            return redirect('/');
        }
    }
}
