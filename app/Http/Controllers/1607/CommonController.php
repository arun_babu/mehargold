<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class CommonController extends Controller
{
    //
    public function s3Manager($imageobject, $code_string)
    {
        $fontHeight = getimagesize($imageobject)[1];
        $imgPath = $imageobject;
        $image = '';
        if ($imageobject->extension() == 'jpeg') {
            $image = imagecreatefromjpeg($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefrompng($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefromgif($imgPath);
        }
//        $color = imagecolorallocate($image, 0, 0, 0);
        $string = strtoupper($code_string);
//        $x = 2;
//        $y = $fontHeight - 20;
//        $font = public_path('fonts/calibri.ttf');
//        $bg_width = strlen($string) * 16;
//        $color1 = imagecolorallocate($image, 225, 224, 224);
//        imagefilledrectangle($image, $x, $y, $bg_width, $y - 20, $color1);

//        imagettftext($image, 15, null, $x, $y, $color, $font, $string);
//        imagestring($image, $fontSize, $x, $y, $string, $color);
//        ob_start();
//        imagejpeg($image);
//        $image_contents = ob_get_clean();
        $textsize = $string . ' ';
        $image_contents = $this->mergeImage($image, $textsize,10,25);
//S3-----
        $path = time() . $imageobject->getClientOriginalName();
        $filePath = 'images/' . date("Y") . '/' . date("m") . '/' . $path;
        $t = Storage::disk('s3')->put($filePath, $image_contents);
//        $imageName = Storage::disk('s3')->url($filePath);

        return $filePath;
    }

    public function s3Media($Path, $image)
    {
        $t = Storage::disk('s3')->put($Path, $image);
        $Path_s3 = Storage::disk('s3')->url($Path);
        return $Path;
    }


    public function s3MediaWithCode($Path, $imageobject, $code_string)
    {
        $fontHeight = getimagesize($imageobject)[1];
        $imgPath = $imageobject;
        $image = '';
        if ($imageobject->extension() == 'jpeg') {
            $image = imagecreatefromjpeg($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefrompng($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefromgif($imgPath);
        }
        $string = strtoupper($code_string);


//        imagettftext($image, 15, null, $x, $y, $color, $font, $string);
//        ob_start();
//        imagejpeg($image);
//        $image_contents = ob_get_clean();
//S3-----
        $textsize = $string . ' ';
        $image_contents = $this->mergeImage($image, $textsize);

        $t = Storage::disk('s3')->put($Path, $image_contents);
        $Path_s3 = Storage::disk('s3')->url($Path);
        return $Path;
    }


    public function s3MediaWithCodeExtra($Path, $imageobject, $code_string, $height)
    {
        $info = getimagesize($Path);
        $extension = image_type_to_extension($info[2]);
        $ext = str_replace('.', '', $extension);
        if ($ext == 'jpeg' || $ext == 'jpg') {
            $image = imagecreatefromjpeg($Path);
        } else if ($ext == 'png') {
            $image = imagecreatefrompng($Path);
        } else if ($ext == 'png') {
            $image = imagecreatefromgif($Path);
        }
        $string = strtoupper($code_string);
        $textsize = $string . ' ';
        $image = $this->mergeImage($image, $textsize);
//S3-----
        $t = Storage::disk('s3')->put($Path, $image);
        $Path_s3 = Storage::disk('s3')->url($Path);
        return $Path;
    }


    public function mergeImage($image, $textsize, $inputsize = 10, $height_im = 10)
    {
        $font = public_path('fonts/calibri.ttf');
// Create the next bounding box for the second text
        $bbox = imagettfbbox($inputsize, 0, $font, $textsize);
        // Create a 300x150 image
        $im = imagecreatetruecolor($bbox[4], $height_im);
        $black = imagecolorallocate($im, 0, 0, 0);
        $white = imagecolorallocate($im, 255, 255, 255);
//        $im = $image;
// Set the background to be white
        imagefilledrectangle($im, 0, 0, 299, 299, $white);
// Path to our font file
// Set the cordinates so its next to the first text
        $x = $bbox[0] + (imagesx($im) / 2) - ($bbox[4] / 2);
        $y = $bbox[1] + (imagesy($im) / 2) - ($bbox[5] / 2);
// Write it
        imagettftext($im, $inputsize, 0, $x, $y, $black, $font, $textsize);
// Output to browser
//            imagepng($im);
//        imagedestroy($im);
        $stamp = $im;
        $im = $image;
// Set the margins for the stamp and get the height/width of the stamp image
        $marge_right = 5;
        $marge_bottom = 5;
        $sx = imagesx($stamp);
        $sy = imagesy($stamp);
// Copy the stamp image onto our photo using the margin offsets and the photo
// width to calculate positioning of the stamp.
        imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
// Output and free memory
//        header('Content-type: image/png');
//        imagepng($im);
        ob_start();
        imagepng($im);
        $image_contents = ob_get_clean();
        return $image_contents;
    }

}
