<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class CommonController extends Controller
{
    //
    public function s3Manager($imageobject, $code_string)
    {
        $fontHeight = getimagesize($imageobject)[1];
        $imgPath = $imageobject;
        $image = '';
        if ($imageobject->extension() == 'jpeg') {
            $image = imagecreatefromjpeg($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefrompng($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefromgif($imgPath);
        }
        $color = imagecolorallocate($image, 0, 0, 0);
        $string = strtoupper($code_string);
        $x = 2;
        $y = $fontHeight - 20;
        $font = public_path('fonts/calibri.ttf');
        $bg_width = strlen($string) * 16;
        $color1 = imagecolorallocate($image, 225, 224, 224);
        imagefilledrectangle($image, $x, $y, $bg_width, $y-20, $color1);

        imagettftext($image, 15, null, $x, $y, $color, $font, $string);
//        imagestring($image, $fontSize, $x, $y, $string, $color);
        ob_start();
        imagejpeg($image);
        $image_contents = ob_get_clean();
//S3-----
        $path = time() . $imageobject->getClientOriginalName();
        $filePath = 'images/' . date("Y") . '/' . date("m") . '/' . $path;
        $t = Storage::disk('s3')->put($filePath, $image_contents);
//        $imageName = Storage::disk('s3')->url($filePath);

        return $filePath;
    }

    public function s3Media($Path, $image)
    {
        $t = Storage::disk('s3')->put($Path, $image);
        $Path_s3 = Storage::disk('s3')->url($Path);
        return $Path;
    }


    public function s3MediaWithCode($Path, $imageobject, $code_string)
    {
        $fontHeight = getimagesize($imageobject)[1];
        $imgPath = $imageobject;
        $image = '';
        if ($imageobject->extension() == 'jpeg') {
            $image = imagecreatefromjpeg($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefrompng($imgPath);
        } else if ($imageobject->extension() == 'png') {
            $image = imagecreatefromgif($imgPath);
        }
        $color = imagecolorallocate($image, 0, 0, 0);
        $string = strtoupper($code_string);
        $x = 2;
        $y = $fontHeight - 20;
        $font = public_path('fonts/calibri.ttf');
        imagettftext($image, 15, null, $x, $y, $color, $font, $string);
        ob_start();
        imagejpeg($image);
        $image_contents = ob_get_clean();
//S3-----
        $t = Storage::disk('s3')->put($Path, $image_contents);
        $Path_s3 = Storage::disk('s3')->url($Path);
        return $Path;
    }


    public function s3MediaWithCodeExtra($Path, $imageobject, $code_string, $height)
    {
        $info = getimagesize($Path);
        $extension = image_type_to_extension($info[2]);
        $ext = str_replace('.', '', $extension);
        $fontHeight = $height;
        $imgPath = $imageobject;
        if ($ext == 'jpeg' || $ext == 'jpg') {
            $image = imagecreatefromjpeg($Path);
        } else if ($ext == 'png') {
            $image = imagecreatefrompng($Path);
        } else if ($ext == 'png') {
            $image = imagecreatefromgif($Path);
        }

        $color = imagecolorallocate($image, 0, 0, 0);
        $string = strtoupper($code_string);
        $x = 2;
        $ratio = ($fontHeight * 55) / 100;
        $y = $ratio;
        $font = public_path('fonts/calibri.ttf');
        $size = ($fontHeight * 1.5) / 100;
        imagettftext($image, $size, null, $x, $y, $color, $font, $string);

        ob_start();
        imagejpeg($image);
        $image_contents = ob_get_clean();
//S3-----
        $t = Storage::disk('s3')->put($Path, $image_contents);
        $Path_s3 = Storage::disk('s3')->url($Path);

        return $Path;
    }

}
