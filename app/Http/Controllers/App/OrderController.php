<?php

namespace App\Http\Controllers\App;

use paytm\paytmchecksum\PaytmChecksum;
use Validator;
use Mail;
use DB;
use DateTime;
use Hash;
use Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Carbon;
use Log;
use Lang;
use App\Models\AppModels\Orders;

use App\Http\Controllers\App\PaytmController;


class OrderController extends Controller
{

    //hyperpaytoken
    public function hyperpaytoken(Request $request)
    {
        $orderResponse = Orders::hyperpaytoken($request);
        print $orderResponse;
    }


    //hyperpaypaymentstatus
    public function hyperpaypaymentstatus(Request $request)
    {
        $req = array('language_id' => 1);
        $payments_setting = Orders::payments_setting_for_hyperpay($req);

        //check envinment
        if ($payments_setting['userid']->environment == '0') {
            $env_url = "https://test.oppwa.com";
        } else {
            $env_url = "https://oppwa.com";
        }

        $url = $env_url . $request->resourcePath;
        $data = "authentication.userId=" . $payments_setting['userid']->value;
        "&authentication.password=" . $payments_setting['password']->value;
        "&authentication.entityId=" . $payments_setting['entityid']->value;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        //print_r($responseData);
        $data = json_decode($responseData);

        if (preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $data->result->code)) {
            $transaction_id = $data->ndc;
            $orders_id = DB::table('orders')->insertGetId(
                ['transaction_id' => $transaction_id,
                    'order_information' => $responseData
                ]);
            return redirect('app/paymentsuccess?data=' . $responseData);
        } else {
            return redirect('app/paymenterror');
        }

    }

    //paymentsuccess
    public function paymentsuccess()
    {
    }

    //paymenterror
    public function paymenterror()
    {
    }

    //generate token
    public function generatebraintreetoken(Request $request)
    {
        $orderResponse = Orders::generatebraintreetoken($request);
        print $orderResponse;
    }

    //instamojoToken
    public function instamojoToken()
    {
        $req = (object)array('language_id' => 1);
        $payments_setting = Orders::payments_setting_for_instamojo($req);
        $instamojo_client_id = $payments_setting['client_id']->value;
        $instamojo_client_secret = $payments_setting['client_secret']->value;
        $instamojo = new InstamojoController($instamojo_client_id, $instamojo_client_secret);
        $clientToken = $instamojo->getToken();
        print $clientToken;
    }

    //get default payment method
    public function getpaymentmethods(Request $request)
    {
        $orderResponse = Orders::getpaymentmethods($request);
        print $orderResponse;
    }

    //get shipping / tax Rate
    public function getrate(Request $request)
    {
        $orderResponse = Orders::getrate($request);
        print $orderResponse;
    }

    //get coupons
    public function getcoupon(Request $request)
    {
        $orderResponse = Orders::getcoupon($request);
        print $orderResponse;
    }

    //addtoorder
    public function addtoorder(Request $request)
    {
        $orderResponse = Orders::addtoorder($request);
        print $orderResponse;
    }


    //getorders
    public function getorders(Request $request)
    {
        $orderResponse = Orders::getorders($request);
        print $orderResponse;
    }

    public function get_client_ip_env()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    //updatestatus
    public function updatestatus(Request $request)
    {
        $orderResponse = Orders::updatestatus($request);
        print $orderResponse;
    }

    public function generatpaytmhashes(Request $request)
    {


        $checkSum = "";
        // below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
        $findme = 'REFUND';
        $findmepipe = '|';

        $paramList = array();

        $obj = new \stdClass;
        $obj->language_id = 1;

        $payments_setting = Orders::payments_setting_for_paytm($obj);
        $orderId = $request->order_id;
        $customers_id = $request->customers_id;
        $amount = $request->amount;
//        live
        define('PAYTM_MERCHANT_KEY', $payments_setting['paytm_key']->value);
        $paramList["MID"] = $payments_setting['paytm_mid']->value;
//        test
//        define('PAYTM_MERCHANT_KEY', 'is%#vydf!tYLMlHB');
//        $paramList["MID"] = 'MEHARG05179183110516';

        $paramList["ORDER_ID"] = $orderId;
        $paramList["CUST_ID"] = $customers_id;
        $paramList["INDUSTRY_TYPE_ID"] = 'ECommerce';
        $paramList["CHANNEL_ID"] = 'WAP';
        $paramList["TXN_AMOUNT"] = $amount;
        $paramList["WEBSITE"] = 'DEFAULT';
        $paramList["CALLBACK_URL"] = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" . $orderId;


//        $paramList["CALLBACK_URL"] = 'https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=' . $orderId;

//        $paramList["CALLBACK_URL"] = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" . $orderId;
//        MID=  MEHARG05179183110516
//        ORDER_ID=  972881668a8046f88db708d09d163e31
//        CUST_ID=  1a029cbebbd742df91c84c6e49b95d9f
//        CHANNEL_ID=  WAP
//        TXN_AMOUNT=  293.14
//        WEBSITE=  WEBSTAGING
//        CALLBACK_URL=  https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=
//        CHECKSUMHASH=  6lQnM4sCHpk23xxxMd8UFRZXoLXvb+FN6p09jnctZlYaSpNXetI16pAcSnOAzsQAMG1QT4Mdr4cU0HpKm61k0oJjHykWgKt+2aUDLff4D9k=
////     INDUSTRY_TYPE_ID=  Retail


        $paytmParams = array();
        if (isset($request->new)) {

            $paytmParams["body"] = array(
                "requestType" => "Payment",
                "mid" => "MEHARG05179183110516",
                "websiteName" => "WEBSTAGING",
                "orderId" => $orderId,
                "callbackUrl" => $paramList["CALLBACK_URL"],
                "txnAmount" => array(
                    "value" => $request->amount,
                    "currency" => "INR",
                ),
                "userInfo" => array(
                    "custId" => $request->customers_id,
                ),
            );
            $checksum = PaytmChecksum::generateSignature(json_encode($paytmParams["body"], JSON_UNESCAPED_SLASHES), "is%#vydf!tYLMlHB");
            $data = array("CHECKSUMHASH" => $checksum, "ORDER_ID" => $orderId, "payt_STATUS" => "1");
            $responseData = array('success' => '1', 'data' => $data, 'message' => "Checksum Hashes is returned");
            $orderResponse = json_encode(($responseData), JSON_UNESCAPED_SLASHES);
            print $orderResponse;
//            $paytmParams["head"] = array(
//                "signature" => $checksum
//            );
//            $post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);
//
//            /* for Staging */
//            $url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid=MEHARG05179183110516&orderId=" . $orderId;
//            $ch = curl_init($url);
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
//            $response = curl_exec($ch);
//            var_dump($response);
//            die();
//            print_r($response);


        } else {
            foreach ($_POST as $key => $value) {
                $pos = strpos($value, $findme);
                $pospipe = strpos($value, $findmepipe);
                if ($pos === false || $pospipe === false) {
                    $paramList[$key] = $value;
                }
            }
            $paytmClass = new PaytmController();
            //Here checksum string will return by getChecksumFromArray() function.
            $checkSum = $paytmClass->getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY);
            $data = array("CHECKSUMHASH" => $checkSum, "ORDER_ID" => $orderId, "payt_STATUS" => "1");
            $responseData = array('success' => '1', 'data' => $data, 'message' => "Checksum Hashes is returned");
            $orderResponse = json_encode(($responseData), JSON_UNESCAPED_SLASHES);
            print $orderResponse;
        }
    }

}
